module.exports = {
    isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        return res.redirect('/authenticate');
    },

    isNotLoggedIn(req, res, next) {
        if (!req.isAuthenticated()) {
            return next();
        }
        return res.redirect('/home');
    },

    passwordSection(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'arquitecto' || (req.user.RolNombre).toLowerCase() === 'gestor' || (req.user.RolNombre).toLowerCase() === 'administrador') {
            return next();
        }
        return res.redirect('/client/password');
    },

    clientsSection(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'arquitecto' || (req.user.RolNombre).toLowerCase() === 'gestor') {
            return next();
        }
        return res.createError(404);
    },

    projectsSection(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'arquitecto') {
            return next();
        }
        return res.createError(404);
    },

    financeSection(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'administrador') {
            return next();
        }
        if ((req.user.RolNombre).toLowerCase() === 'arquitecto'){
            return res.redirect(`/finance/${req.user.idUsuario}`);
        }
        return res.createError(404);
    },

    suppliersSection(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'arquitecto' || (req.user.RolNombre).toLowerCase() === 'administrador') {
            return next();
        }
        return res.createError(404);
    },

    servicesSection(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'gestor') {
            return next();
        }
        return res.createError(404);
    },

    usersSection(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'gestor') {
            return next();
        }
        return res.createError(404);
    },

    hasClientPermission(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'cliente') {
            return next();
        }
        return res.createError(404);
    },

    hasEmployeePermission(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'arquitecto' || (req.user.RolNombre).toLowerCase() === 'gestor' || (req.user.RolNombre).toLowerCase() === 'administrador') {
            return next();
        }
        return res.createError(404);
    },

    hasArchitectPermission(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'arquitecto') {
            return next();
        }
        return res.createError(404);
    },

    hasGestorPermission(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'gestor') {
            return next();
        }
        return res.createError(404);
    },

    hasAdminPermission(req, res, next) {
        if ((req.user.RolNombre).toLowerCase() === 'administrador') {
            return next();
        }
        return res.createError(404);
    }
};