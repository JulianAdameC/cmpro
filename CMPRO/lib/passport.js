const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const pool = require('../databasePool');
const helpers = require('./helpers');

passport.use('local.signup', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) => {
    const { fullname } = req.body;
    let newUser = {
        username,
        password,
        fullname
    };
    newUser.password = await helpers.encryptPassword(password);
    // Saving in the Database
    const result = await pool.query('INSERT INTO usuario SET ? ', newUser);
    newUser.id = result.insertId;
    return done(null, newUser);
}));

//Empleado
passport.use('local.signin', new LocalStrategy({
    usernameField: 'correo',
    passwordField: 'contrasena',
    passReqToCallback: true
}, async (req, username, password, done) => {
    const rows = await pool.query(
        'SELECT idUsuario, Correo, Contrasena, Nombre, Apellido FROM usuario INNER JOIN empleado ON usuario.idUsuario=empleado.Usuario WHERE Correo = ?',
        [username]
    );
    if (rows.length > 0) {
        const user = rows[0];
        const validPassword = await helpers.matchPassword(password, user.Contrasena);
        if (validPassword) {
            done(null, user, req.flash('success', `Bienvenido, ${user.Nombre} ${user.Apellido}`));
        }
        else {
            done(null, false, req.flash('error', 'Usuario o contraseña incorrectos. Revise los datos ingresados.'));
        }
    }
    else {
        return done(null, false, req.flash('error', 'Usuario o contraseña incorrectos. Revise los datos ingresados.'));
    }
}));

//Cliente
passport.use('local.signinClient', new LocalStrategy({
    usernameField: 'correo',
    passwordField: 'contrasena',
    passReqToCallback: true
}, async (req, username, password, done) => {
    const rows = await pool.query(
        'SELECT idUsuario, Correo, Contrasena, Nombre, Apellido FROM usuario INNER JOIN cliente ON usuario.idUsuario=cliente.Usuario WHERE Correo = ?',
        [username]
    );
    if (rows.length > 0) {
        const user = rows[0];
        const validPassword = await helpers.matchPassword(password, user.Contrasena);
        if (validPassword) {
            done(null, user, req.flash('success', `Bienvenido, ${user.Nombre} ${user.Apellido}`));
        }
        else {
            done(null, false, req.flash('error', 'Usuario o contraseña incorrectos. Revise los datos ingresados.'));
        }
    }
    else {
        return done(null, false, req.flash('error', 'Usuario o contraseña incorrectos. Revise los datos ingresados.'));
    }
}));

//Empleado
passport.use('profile.editPassword', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'actualPassword',
    passReqToCallback: true
}, async (req, correo, password, done) => {
    const rows = await pool.query('SELECT * FROM usuario WHERE Correo = ?', [correo]);
    if (rows.length > 0) {
        const user = rows[0];
        const validPassword = await helpers.matchPassword(password, user.Contrasena);
        if (validPassword) {
            const { newPassword } = req.body;
            const protectedPassword = await helpers.encryptPassword(newPassword);
            const result = await pool.query('UPDATE usuario SET Contrasena = ? WHERE idUsuario = ?', [protectedPassword, user.idUsuario]);
            done(null, false, req.flash('success', 'Se ha modificado su contraseña'));
        }
        else {
            done(null, false, req.flash('error', 'Contraseña incorrecta'));
        }
    }
}));

//Cliente
passport.use('client.editPassword', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'actualPassword',
    passReqToCallback: true
}, async (req, correo, password, done) => {
    const rows = await pool.query('SELECT * FROM usuario WHERE Correo = ?', [correo]);
    if (rows.length > 0) {
        const user = rows[0];
        const validPassword = await helpers.matchPassword(password, user.Contrasena);
        if (validPassword) {
            const { newPassword } = req.body;
            const protectedPassword = await helpers.encryptPassword(newPassword);
            const result = await pool.query('UPDATE usuario SET Contrasena = ? WHERE idUsuario = ?', [protectedPassword, user.idUsuario]);
            done(null, false, req.flash('success', 'Se ha modificado su contraseña'));
        }
        else {
            done(null, false, req.flash('error', 'Contraseña incorrecta'));
        }
    }
}));

passport.serializeUser((user, done) => {
    done(null, user.idUsuario);
});

passport.deserializeUser(async (id, done) => {
    const rows = await pool.query(
        `SELECT idUsuario, Correo, Rol, rol.Nombre AS RolNombre 
        FROM usuario 
        INNER JOIN rol ON usuario.Rol=rol.idRol 
        WHERE idUsuario = ?`, 
        [id]
    );
    done(null, rows[0]);
});