const { format } = require('timeago.js');

const helpers = {};

helpers.timeago = (timestamp) => {
    return format(timestamp);
};

helpers.fixDate = (date) => {
    const formatDate = date.toString().split(' ');
    return `${formatDate[1]} ${formatDate[2]} ${formatDate[3]} ${formatDate[4]}`;
};

helpers.fixDateProject = (date) => {
    if (date === '0000-00-00 00:00:00' || date === null) {
        console.log(date)
        return 'Sin definir';
    }

    const formatDate = date.toString().split(' ');
    return `${formatDate[1]} ${formatDate[2]} ${formatDate[3]}`;
};

helpers.fixEditProject = (date) => {
    const formatDate = date.toString().split(' ');
    let mes = '';
    switch (formatDate[1]) {
        case 'Jan':
            mes = '01'
            break;

        case 'Feb':
            mes = '02'
            break;

        case 'Mar':
            mes = '03'
            break;

        case 'Apr':
            mes = '04'
            break;

        case 'May':
            mes = '05'
            break;

        case 'Jun':
            mes = '06'
            break;

        case 'Jul':
            mes = '07'
            break;

        case 'Aug':
            mes = '08'
            break;

        case 'Sep':
            mes = '09'
            break;

        case 'Oct':
            mes = '10'
            break;

        case 'Nov':
            mes = '11'
            break;

        default:
            mes = '12'
            break;
    }
    return `${formatDate[3]}-${mes}-${formatDate[2]}`;
};

helpers.fixStatus = (estatus) => {
    let estado = '';

    if (estatus === 1)
        estado = 'Activo';
    else
        estado = 'Inactivo'

    return estado;
};

helpers.searchImage = (imgRoute) => {
    if (imgRoute !== '') {
        return imgRoute;
    }
    return '/img/yomero.png';
};

helpers.searchImageProject = (imgRoute) => {
    if (imgRoute !== '') {
        return imgRoute;
    }
    return '/img/proyecto.jpg';
};

helpers.searchContrato = (contrato) => {
    if (contrato === '' || contrato === null)
        return '#'

    return contrato
};

helpers.colorfinanza=(movimiento)=> {
    if(movimiento.toLowerCase()==='ingreso'){
        return "text-success"
    }
    else {
        return "text-danger"
    }
}

module.exports = helpers;