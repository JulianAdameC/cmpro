const mysql = require('mysql');
const { promisify } = require('util');
const { database } = require('./keys');

const connection = mysql.createConnection(database);

connection.connect(function (err) {
    if (err) {
        console.log(err.message);
        return;
    }

    console.log('DB ' + connection.state);
});

//promisify conn querys
connection.query = promisify(connection.query);

//promisify conn transactions
connection.beginTransaction = promisify(connection.beginTransaction);

//promisify conn commits
connection.commit = promisify(connection.commit);

//promisify conn rollbacks
connection.rollback = promisify(connection.rollback);

//promisify conn destroy
connection.destroy = promisify(connection.destroy);

module.exports = connection;