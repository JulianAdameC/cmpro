const express = require('express');
const morgan = require('morgan');
const exphbs = require('express-handlebars');
const path = require('path');
const flash = require('connect-flash');
const session = require('express-session');
const MySQLStore = require('express-mysql-session');
const passport = require('passport');

const { database } = require('./keys');
const multerClient = require('multer');


// const clientStorage = multerClient.diskStorage({
//     destination: path.join(__dirname, 'public/img/client'), 
//     filename: (req, file, cb) => {
//         const splitName = file.originalname.split('.');
//         const extension = splitName[splitName.length-1];
//         cb(null, `client${req.cliente}.${extension}`)
//     }
// });

//initializations
const app = express();
require('./lib/passport');

//settings
app.set('port', process.env.PORT || 4000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs',
    helpers: require('./lib/handlebars')
}));
app.set('view engine', '.hbs');

//middleware
app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(session({
    secret: 'codeneosession',
    resave: false,
    saveUninitialized: false,
    store: new MySQLStore(database)
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

// app.use(multerClient({
//     storage: clientStorage, 
//     dest: path.join(__dirname, 'public/img/client')
// }).single('client-foto'));

//global variables
app.use((req, res, next) => {
    app.locals.success = req.flash('success');
    app.locals.error = req.flash('error');
    app.locals.user = req.user;
    next();
});

//routes
app.use(require('./routes/index'));
app.use(require('./routes/authentication'));
app.use('/clients', require('./routes/clients'));
app.use('/finance', require('./routes/finance'));
app.use('/profile', require('./routes/profile'));
app.use('/projects', require('./routes/projects'));
app.use('/services', require('./routes/services'));
app.use('/suppliers', require('./routes/suppliers'));
app.use('/users', require('./routes/users'));
app.use('/other', require('./routes/other'));
app.use('/client', require('./routes/clientProfile'));
app.use('/noPermission', require('./routes/noPermission'));

//public
app.use(express.static(path.join(__dirname, 'public')));

//staring the server
app.listen(app.get('port'), () => {
    console.log('Server on port ', app.get('port'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    /*res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};*/

    // render the error page
    res.status(err.status || 500);
    res.render('error/404');
});