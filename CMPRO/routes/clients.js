const express = require('express');
const router = express.Router();

const pool = require('../databasePool');
const helpers = require('../lib/helpers');
const { isLoggedIn, clientsSection } = require('../lib/authentication');

const path = require('path');
const multer = require('multer');

const fs = require('fs');
const { v4: uuidv4 } = require('uuid')
let ruta = ''

const storage = multer.diskStorage({
    destination: path.join(__dirname, '../public/img/client'),
    filename: (req, file, cb) => {
        ruta = uuidv4() + path.extname(file.originalname).toLocaleLowerCase();
        cb(null, ruta);
    }
});
/*  
    Considerar también un método parecido, para saber 
    si el usuario tiene el rol para la acción, como:

    -------------------------
    //en este archivo
    const {hasArchitectPermission} = require('../lib/authentication');

    //en ../lib/authentication
    hasArchitectPermission(req, res, next) {
        if(req.user.tipoUsuario === 'arquitecto'){
            return next();
        }
        return res.redirect('/noPermission/architect');
    } 
    -------------------------
*/

/*---------------List clients---------------*/
//Agregar parámetros de isLogged y rol
//List all clients
router.get('/', isLoggedIn, clientsSection, async (req, res) => {
    const clients = await pool.query(
        'SELECT idCliente, Nombre, Apellido, Correo, Telefono, RazonSocial, RFC, Imagen ' +
        'FROM cliente ' +
        'INNER JOIN usuario ON Usuario = idUsuario ORDER BY RazonSocial'
    );
    res.render('clients/list', { clients: clients });
});

/*---------------Add client---------------*/
router.get('/add', isLoggedIn, clientsSection, (req, res) => {
    res.render('clients/add');
});

router.post('/add', async (req, res) => {
    const rows = await pool.query('SELECT idRol FROM rol WHERE Nombre = ?', ['Cliente']);
    const rolCliente = rows[0];

    const { Correo, Contrasena } = req.body;
    let usuario = {
        Correo,
        Contrasena,
    };
    usuario.Contrasena = await helpers.encryptPassword(Contrasena);
    usuario.Rol = rolCliente.idRol;
    const result = await pool.query('INSERT INTO usuario SET ?', [usuario]);

    const { Nombre, Apellido, Telefono, RFC, RazonSocial } = req.body;
    let cliente = { Nombre, Apellido, Telefono, RFC, RazonSocial };
    cliente.Usuario = result.insertId;
    cliente.AgregadoPor = req.user.idUsuario;
    cliente.Imagen = "";
    const result2 = await pool.query(
        'INSERT INTO cliente SET ?', [cliente]
    );
    req.flash("success", "Se ha agregado al cliente correctamente");
    res.redirect("/clients");
});

/*---------------Edit client---------------*/
router.get('/edit/:idCliente', isLoggedIn, clientsSection, async (req, res) => {
    const { idCliente } = req.params;
    const cliente = await pool.query(
        'SELECT idCliente, Nombre, Apellido, Telefono, RFC, RazonSocial, Correo ' +
        'FROM cliente ' +
        'INNER JOIN usuario ON cliente.Usuario = idUsuario ' +
        'WHERE idCliente = ?',
        [idCliente]
    );
    res.render('clients/edit', { cliente: cliente[0] });
});

router.post('/edit/:idCliente', async (req, res) => {
    const { idCliente } = req.params;
    const rows = await pool.query('SELECT Usuario FROM cliente WHERE idCliente = ?', [idCliente]);
    const idUsuario = rows[0].Usuario;

    const { Correo } = req.body;
    let usuario = {
        Correo
    };
    const result = await pool.query('UPDATE usuario SET ? WHERE idUsuario = ?', [usuario, idUsuario]);

    const { Nombre, Apellido, Telefono, RFC, RazonSocial } = req.body;
    let cliente = { Nombre, Apellido, Telefono, RFC, RazonSocial };
    const result2 = await pool.query('UPDATE cliente SET ? WHERE idCliente = ?', [cliente, idCliente]);
    req.flash("success", "Se han modificado los datos correctamente");
    res.redirect("/clients");
});

router.post('/edit', async (req, res) => {
    const { idCliente } = req.body;
    const rows = await pool.query('SELECT Usuario FROM cliente WHERE idCliente = ?', [idCliente]);
    const idUsuario = rows[0].Usuario;

    const { Correo } = req.body;
    let usuario = {
        Correo
    };
    const result = await pool.query('UPDATE usuario SET ? WHERE idUsuario = ?', [usuario, idUsuario]);

    const { Nombre, Apellido, Telefono, RFC, RazonSocial } = req.body;
    let cliente = { Nombre, Apellido, Telefono, RFC, RazonSocial };
    const result2 = await pool.query('UPDATE cliente SET ? WHERE idCliente = ?', [cliente, idCliente]);
    req.flash("success", "Se han modificado los datos correctamente");
    res.redirect("/clients");
});

//Edit client photo
router.get('/image/:idCliente', async (req, res) => {
    const { idCliente } = req.params;
    const imagen = await pool.query(
        'SELECT Imagen FROM cliente WHERE idCliente = ?',
        [idCliente]
    );
    res.json(imagen);
});

const photo = multer({
    storage: storage,
    dest: path.join(__dirname, '../public/img/client'),
    limits: { fileSize: 2000000 },
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/;
        const mimetype = fileTypes.test(file.mimetype);
        const extName = fileTypes.test(path.extname(file.originalname));
        if (mimetype && extName) {
            return cb(null, true);
        }
        cb('Error: File must be an image');
    }
}).single('cliente-foto');

router.post('/photo', photo, async (req, res) => {
    const { idCliente } = req.body;
    // const rows = await pool.query('SELECT idCliente FROM cliente WHERE Usuario = ?', [req.user.idUsuario]);
    // if (rows.length > 0) {
    //     const empleado = rows[0];
    const rows = await pool.query('SELECT Imagen FROM cliente WHERE idCliente = ?', [idCliente]);
    let imagen = rows[0].Imagen;
    if (imagen !== null && imagen !== '') {
        var filePath = path.join(__dirname, `../public${imagen}`);
        fs.unlinkSync(filePath);
    }

    let Imagen = `/img/client/${ruta}`;

    await pool.query('UPDATE cliente SET Imagen = ? WHERE idCliente = ?', [Imagen, idCliente]);
    req.flash("success", "Se ha modificado la foto correctamente");
    res.redirect('/clients');
    // }
});

module.exports = router;