const express = require('express');
const router = express.Router();

router.get('/architect', (req, res) => {
    req.flash('error', 'No tienes permiso para realizar esta acción');
    res.redirect('/index/architect');
});

module.exports = router;