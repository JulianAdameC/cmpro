const express = require('express');
const router = express.Router();

const pool = require('../databasePool');
const { isLoggedIn, suppliersSection, hasAdminPermission } = require('../lib/authentication');
/*  
    Considerar también un método parecido, para saber 
    si el usuario tiene el rol para la acción, como:

    -------------------------
    //en este archivo
    const {hasArchitectPermission} = require('../lib/authentication');

    //en ../lib/authentication
    hasArchitectPermission(req, res, next) {
        if(req.user.tipoUsuario === 'arquitecto'){
            return next();
        }
        return res.redirect('/noPermission/architect');
    } 
    -------------------------
*/

/*---------------List suppliers---------------*/
//Agregar parámetros de isLogged y rol
//List all suppliers
router.get('/', isLoggedIn, suppliersSection, async (req, res) => {
    const cities = await pool.query(
        'SELECT DISTINCT * '+
        'FROM ciudad order by Ciudad'
    );
    const states = await pool.query(
        'SELECT DISTINCT * '+
        'FROM estado order by Estado'
    );
    const suppliers = await pool.query(
        'SELECT * ' +
        'FROM proveedor ORDER BY nombre'
    );
    res.render('suppliers/list', { suppliers: suppliers, cities:cities, states:states });
});



/*---------------Add supplier---------------*/

router.post('/add', async (req, res) => {
    const {
        Proveedor,
        TipoServicio,
        Descripcion,
        Ciudad,
        Colonia,
        Calle,
        Numero,
        CP,
        Correo,
        Telefono,
        RFC,
        Calif   
    } =req.body;
    const newDirection={
        Ciudad,
        Colonia,
        Calle,
        Numero,
        CP,
        Telefono
    };
    const result= await pool.query('INSERT INTO direccion SET ?',[newDirection]);
    const newSupplier={
        Nombre: Proveedor,
        Tipo: TipoServicio,
        Descripcion,
        Correo,
        Telefono,
        AgregadoPor: req.user.idUsuario,
        RFC,
        Direccion: result.insertId,
        Score: Calif
    }
    await pool.query('INSERT INTO proveedor SET ?',[newSupplier]);
    req.flash("success", "Se ha agregado el proveedor correctamente");


    res.redirect('/suppliers');
    
});


/*---------------Edit supplier´s info---------------*/
router.post('/edit', async (req, res) => {
    const { Edit_ID } = req.body;
    const idDireccion = await pool.query('SELECT Direccion FROM proveedor WHERE idProveedor = ?', [Edit_ID]);
    const {
        Proveedor,
        TipoServicio,
        Descripcion,
        Ciudad,
        Colonia,
        Calle,
        Numero,
        CP,
        Correo,
        Telefono,
        RFC,
        Calif   
    } =req.body;
    const direction={
        Ciudad,
        Colonia,
        Calle,
        Numero,
        CP,
        Telefono
    };
    const result= await pool.query('UPDATE direccion SET ? WHERE idDireccion = ?',[direction, idDireccion[0].Direccion]);
    const supplier={
        Nombre: Proveedor,
        Tipo: TipoServicio,
        Descripcion,
        Correo,
        Telefono,
        RFC,
        Score: Calif
    }
    await pool.query('UPDATE proveedor SET ? WHERE idProveedor = ?',[supplier, Edit_ID]);
    req.flash("success", "Se han editado los datos correctamente");

    res.redirect('/suppliers');
    
});

router.get('/info/:idProveedor', async(req, res) => {
    const { idProveedor } = req.params;
    const supplier = await pool.query(
        `SELECT Nombre, Tipo, Descripcion, Correo, Proveedor.Telefono, RFC, Score, direccion.Ciudad, Colonia, Calle, Numero, CP, ciudad.Estado 
        FROM proveedor 
        INNER JOIN direccion ON proveedor.Direccion=direccion.idDireccion 
        INNER JOIN ciudad ON direccion.Ciudad=ciudad.idCiudad 
        INNER JOIN estado ON ciudad.Estado=estado.idEstado 
        WHERE idProveedor=?`, 
        [idProveedor]
    );
    res.json(supplier)
});

/*---------------List supplier´s services---------------*/
//List all services
router.get('/:idProveedor/services', isLoggedIn, suppliersSection, async (req, res) => {
    const { idProveedor } = req.params;
    const nombre = await pool.query(
        'SELECT Nombre FROM proveedor WHERE idProveedor = ?', 
        [idProveedor]
    );
    const tipos = await pool.query('SELECT * FROM tiposervicio');
    const services = await pool.query(
        'SELECT idServicioProveedores, Proveedor, tipo.Servicio AS tipos, sp.Servicio AS nombre, Costo, Descripcion ' +
        'FROM servicioproveedores sp ' +
        'INNER JOIN tiposervicio tipo ON sp.Tipo = tipo.idTiposServicio ' +
        'WHERE Proveedor = ? ' +
        'ORDER BY nombre',
        [idProveedor]
    );
    res.render('suppliers/listService', { nombre: nombre[0], services, tipos, id: idProveedor });
});

router.get('/:idProveedor/type/:idServicioProveedores', async (req, res) => {
    const { idProveedor, idServicioProveedores } = req.params;
    const tipo = await pool.query(
        'SELECT Tipo AS tipo FROM servicioproveedores WHERE idServicioProveedores = ?', 
        [idServicioProveedores]
    );
    res.json( tipo );
});

/*---------------Add service---------------*/
router.get('/:idProveedor/services/add', isLoggedIn, hasAdminPermission, async (req, res) => {
    const { idProveedor } = req.params;
    const tipos = await pool.query('SELECT * FROM tiposervicio');
    const attr = await pool.query(
        'SELECT idAtributo, Atributo, Descripcion_IyE ' +
        'FROM atributo ORDER BY Atributo'
    );
    res.render('suppliers/addService', { tipos, attr, id: idProveedor });
});

router.post('/:idProveedor/services/add', async (req, res) => {
    const { idProveedor } = req.params;
    const {
        Tipo,
        Servicio,
        Costo,
        Descripcion,
        Lista
    } = req.body;
    const newService = {
        Proveedor: idProveedor,
        Tipo,
        Servicio,
        Costo,
        Descripcion
    };
    const lista = JSON.parse(Lista);

    const result = await pool.query('INSERT INTO servicioproveedores SET ?', [newService]);
    const idServicio = result.insertId;
    lista.forEach(async function (el, index) {
        let phsha = {
            Atributo: el.idAtributo,
            Servicio: idServicio
        };
        await pool.query('INSERT INTO proveedorhasserviciohasatributp SET ?', [phsha]);
    });
    // await pool.query('INSERT INTO serviciohasatributo VALUES (?)', [JSON.stringify(sha)]);
    req.flash("success", "Se ha agregado el servicio correctamente");
    res.redirect(`/suppliers/${idProveedor}/services`);
});

/*---------------Edit service---------------*/
// router.get('/:idProveedor/services/edit/:idServicio', isLoggedIn, suppliersSection, async (req, res) => {
//     const { idServicio } = req.params;
//     const tipos = await pool.query('SELECT * FROM tiposervicio');
//     const servicio = await pool.query(
//         'SELECT idServicioProveedores, Tipo, Servicio, Costo, Descripcion ' +
//         'FROM servicioproveedores ' +
//         'WHERE idServicioProveedores = ?',
//         [idServicio]
//     );
//     res.render('suppliers/editService', { tipos, servicio: servicio[0] });
// });

// router.post('/:idProveedor/services/edit/:idServicio', async (req, res) => {
//     const { idProveedor, idServicio } = req.params;
//     const { Tipo, Servicio, Costo, Descripcion } = req.body;
//     let service = { Tipo, Servicio, Costo, Descripcion };
//     const result = await pool.query('UPDATE servicioproveedores SET ? WHERE idServicioProveedores = ?', [service, idServicio]);
//     req.flash("success", "Se han modificado los datos correctamente");
//     res.redirect(`/suppliers/${idProveedor}/services`);
// });

router.post('/:idProveedor/services/edit', async (req, res) => {
    const { idProveedor } = req.params;
    const { Tipo, Servicio, Costo, Descripcion, servprov } = req.body;
    let service = { Tipo, Servicio, Costo, Descripcion };
    const result = await pool.query('UPDATE servicioproveedores SET ? WHERE idServicioProveedores = ?', [service, servprov]);
    req.flash("success", "Se han modificado los datos correctamente");
    res.redirect(`/suppliers/${idProveedor}/services`);
});


/*---------------List attributes---------------*/
//List all attributes
router.get('/:idProveedor/services/:idServicio/attributes', isLoggedIn, suppliersSection, async (req, res) => {
    const { idProveedor, idServicio } = req.params;
    const nombre = await pool.query('SELECT Servicio FROM servicioproveedores WHERE idServicioProveedores = ?', [idServicio]);
    const attr = await pool.query(
        'SELECT idAtributo, atr.Atributo, Descripcion_IyE ' +
        'FROM atributo atr ' +
        'INNER JOIN proveedorhasserviciohasatributp phsha ON atr.idAtributo = phsha.Atributo ' +
        'INNER JOIN servicioproveedores sp ON sp.idServicioProveedores = phsha.Servicio ' +
        'WHERE phsha.Servicio = ? ORDER BY atr.Atributo',
        // 'WHERE sha.Servicio = ? ' +
        // 'ORDER BY sha.Atributo ASC',
        [idServicio]
    );
    res.render('suppliers/listAttr', { nombre: nombre[0], attr, idProveedor, idServicio });
});

/*---------------Add attribute---------------*/
// router.get('/:idCdeServicios/attributes/add', async (req, res) => {
//     const { idCdeServicios } = req.params;
//     res.render('services/addAttr', { idCdeServicios });//Para qué sirve el tipo de servicio
// });

router.post('/:idProveedor/services/attributes/add', async (req, res) => {
    const { idProveedor } = req.params;
    const { Modal_Atributo, Modal_Descripcion } = req.body;
    const newAttr = {
        Atributo: Modal_Atributo,
        Descripcion_IyE: Modal_Descripcion
    };
    await pool.query('INSERT INTO atributo SET ?', [newAttr]);
    req.flash("success", "Se ha agregado el atributo correctamente");
    res.redirect(`/suppliers/${idProveedor}/services`);
});

/*---------------Edit attribute---------------*/
//Edit attribute´s info
router.post('/:idProveedor/services/:idServicio/attributes/info', async (req, res) => {
    const { idProveedor, idServicio } = req.params;
    const { Edit_Atributo, Edit_Descripcion, Edit_ID } = req.body;
    let attr = {
        Atributo: Edit_Atributo,
        Descripcion_IyE: Edit_Descripcion
    };
    const result = await pool.query(
        'UPDATE atributo SET ? WHERE idAtributo = ?',
        [attr, Edit_ID]
    );
    req.flash("success", "Se han modificado los datos correctamente");
    res.redirect(`/suppliers/${idProveedor}/services/${idServicio}/attributes`);
});

//Edit service´s attributes
router.get('/:idProveedor/services/:idServicio/attributes/edit', isLoggedIn, hasAdminPermission, async (req, res) => {
    const { idProveedor, idServicio } = req.params;
    const nombre = await pool.query('SELECT Servicio FROM servicioproveedores WHERE idServicioProveedores = ?', [idServicio]);
    const lista = await pool.query(
        'SELECT idAtributo, Atributo, Descripcion_IyE ' +
        'FROM atributo ORDER BY Atributo'
    );
    const attr = await pool.query(
        'SELECT idAtributo, atr.Atributo, Descripcion_IyE ' +
        'FROM atributo atr ' +
        'INNER JOIN proveedorhasserviciohasatributp phsha ON atr.idAtributo = phsha.Atributo ' +
        'INNER JOIN servicioproveedores sp ON sp.idServicioProveedores = phsha.Servicio ' +
        'WHERE phsha.Servicio = ? ORDER BY atr.Atributo',
        [idServicio]
    );
    res.render('suppliers/editAttr', { lista, attr, idProveedor, idServicio:idServicio, nombre: nombre[0] });
});

router.post('/:idProveedor/services/:idServicio/attributes/edit', async (req, res) => {
    const { idProveedor, idServicio } = req.params;
    const { Lista } = req.body;
    const lista = JSON.parse(Lista);

    await pool.query('DELETE FROM proveedorhasserviciohasatributp WHERE Servicio = ?', [idServicio]);
    lista.forEach(async function (el, index) {
        let phsha = { 
            Atributo: el.idAtributo,
            Servicio: idServicio
        };
        await pool.query('INSERT INTO proveedorhasserviciohasatributp SET ?', [phsha]);
    });
    req.flash("success", "Se han modificado los datos correctamente");
    res.redirect(`/suppliers/${idProveedor}/services/${idServicio}/attributes`);
});

//Checar la base de datos para agregarla 
module.exports = router;