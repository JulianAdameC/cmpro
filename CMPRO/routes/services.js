const express = require('express');
const router = express.Router();

const pool = require('../databasePool');
const { isLoggedIn, servicesSection } = require('../lib/authentication');
/*  
    Considerar también un método parecido, para saber 
    si el usuario tiene el rol para la acción, como:

    -------------------------
    //en este archivo
    const {hasArchitectPermission} = require('../lib/authentication');

    //en ../lib/authentication
    hasArchitectPermission(req, res, next) {
        if(req.user.tipoUsuario === 'arquitecto'){
            return next();
        }
        return res.redirect('/noPermission/architect');
    } 
    -------------------------
*/

/*---------------List services---------------*/
//Agregar parámetros de isLogged y rol
//List all services
router.get('/', isLoggedIn, servicesSection, async (req, res) => {
    const tipos = await pool.query('SELECT * FROM tiposervicio');
    const services = await pool.query(
        'SELECT idCdeServicios, tipo.Servicio AS tipos, cartera.Servicio AS nombre, Costo, Descripcion ' +
        'FROM carteradeservicios cartera ' +
        'INNER JOIN tiposervicio tipo ON cartera.TiposServicio = tipo.idTiposServicio ' +
        // 'WHERE idCdeServicios > 10 ' +
        'ORDER BY nombre'
    );
    res.render('services/listService', { services: services, tipos });
});

router.get('/type/:idCdeServicios', async (req, res) => {
    const { idCdeServicios } = req.params;
    const tipo = await pool.query(
        'SELECT TiposServicio AS tipo FROM carteradeservicios WHERE idCdeServicios = ?', 
        [idCdeServicios]
    );
    res.json( tipo );
});

/*---------------Add service---------------*/
router.get('/add', isLoggedIn, servicesSection, async (req, res) => {
    const tipos = await pool.query('SELECT * FROM tiposervicio');
    const attr = await pool.query(
        'SELECT idAtributo, Atributo, Descripcion_IyE ' +
        'FROM atributo ORDER BY Atributo'
    );
    res.render('services/addService', { tipos, attr });
});

router.post('/add', async (req, res) => {
    const {
        TiposServicio,
        Servicio,
        Costo,
        Descripcion,
        Lista
    } = req.body;
    const newService = {
        TiposServicio,
        Servicio,
        Costo,
        Descripcion
    };
    const lista = JSON.parse(Lista);

    const result = await pool.query('INSERT INTO carteradeservicios SET ?', [newService]);
    const idServicio = result.insertId;
    lista.forEach(async function (el, index) {
        let sha = { 
            Atributo: el.idAtributo,
            Servicio: idServicio
        };
        await pool.query('INSERT INTO serviciohasatributo SET ?', [sha]);
    });
    // await pool.query('INSERT INTO serviciohasatributo VALUES (?)', [JSON.stringify(sha)]);
    req.flash("success", "Se ha agregado el servicio correctamente");
    res.redirect('/services');
});

/*---------------Edit service---------------*/
router.get('/edit/:idCdeServicios', isLoggedIn, servicesSection, async (req, res) => {
    const { idCdeServicios } = req.params;
    const tipos = await pool.query('SELECT * FROM tiposervicio');
    const servicio = await pool.query(
        'SELECT idCdeServicios, TiposServicio, Servicio, Costo, Descripcion ' +
        'FROM carteradeservicios ' +
        'WHERE idCdeServicios = ?',
        [idCdeServicios]
    );
    res.render('services/editService', { tipos, servicio: servicio[0] });
});

router.post('/edit/:idCdeServicios', async (req, res) => {
    const { idCdeServicios } = req.params;
    const { TiposServicio, Servicio, Costo, Descripcion } = req.body;
    let service = { TiposServicio, Servicio, Costo, Descripcion };
    const result = await pool.query('UPDATE carteradeservicios SET ? WHERE idCdeServicios = ?', [service, idCdeServicios]);
    req.flash("success", "Se han modificado los datos correctamente");
    res.redirect("/services");
});

router.post('/edit', async (req, res) => {
    const { cartera } = req.body;
    const { TiposServicio, Servicio, Costo, Descripcion } = req.body;
    let service = { TiposServicio, Servicio, Costo, Descripcion };
    const result = await pool.query('UPDATE carteradeservicios SET ? WHERE idCdeServicios = ?', [service, cartera]);
    req.flash("success", "Se han modificado los datos correctamente");
    res.redirect("/services");
});


/*---------------List attributes---------------*/
//List all attributes
router.get('/:idCdeServicios/attributes', isLoggedIn, servicesSection, async (req, res) => {
    const { idCdeServicios } = req.params;
    const nombre = await pool.query('SELECT Servicio FROM carteradeservicios WHERE idCdeServicios = ?', [idCdeServicios]);
    const attr = await pool.query(
        'SELECT idAtributo, atr.Atributo, Descripcion_IyE ' +
        'FROM atributo atr ' +
        'INNER JOIN serviciohasatributo sha ON atr.idAtributo = sha.Atributo ' +
        'INNER JOIN carteradeservicios cartera ON cartera.idCdeServicios = sha.Servicio ' +
        'WHERE sha.Servicio = ? ORDER BY atr.Atributo',
        // 'WHERE sha.Servicio = ? ' +
        // 'ORDER BY sha.Atributo ASC',
        [idCdeServicios]
    );
    res.render('services/listAttr', { nombre: nombre[0], attr, idCartera: idCdeServicios });
});

/*---------------Add attribute---------------*/
// router.get('/:idCdeServicios/attributes/add', async (req, res) => {
//     const { idCdeServicios } = req.params;
//     res.render('services/addAttr', { idCdeServicios });//Para qué sirve el tipo de servicio
// });

router.post('/attributes/add', async (req, res) => {
    const { Modal_Atributo, Modal_Descripcion } = req.body;
    const newAttr = {
        Atributo: Modal_Atributo,
        Descripcion_IyE: Modal_Descripcion
    };
    await pool.query('INSERT INTO atributo SET ?', [newAttr]);
    req.flash("success", "Se ha agregado el atributo correctamente");
    res.redirect(`/services`);
});

/*---------------Edit attribute---------------*/
//Edit attribute´s info
router.post('/:idCdeServicios/attributes/info', async (req, res) => {
    const { idCdeServicios } = req.params;
    const { Edit_Atributo, Edit_Descripcion, Edit_ID } = req.body;
    let attr = {
        Atributo: Edit_Atributo,
        Descripcion_IyE: Edit_Descripcion
    };
    const result = await pool.query(
        'UPDATE atributo SET ? WHERE idAtributo = ?',
        [attr, Edit_ID]
    );
    req.flash("success", "Se han modificado los datos correctamente");
    res.redirect(`/services/${idCdeServicios}/attributes`);
});

//Edit service´s attributes
router.get('/:idCdeServicios/attributes/edit', isLoggedIn, servicesSection, async (req, res) => {
    const { idCdeServicios } = req.params;
    const nombre = await pool.query('SELECT Servicio FROM carteradeservicios WHERE idCdeServicios = ?', [idCdeServicios]);
    const lista = await pool.query(
        'SELECT idAtributo, Atributo, Descripcion_IyE ' +
        'FROM atributo ORDER BY Atributo'
    );
    const attr = await pool.query(
        'SELECT idAtributo, atr.Atributo, Descripcion_IyE ' +
        'FROM atributo atr ' +
        'INNER JOIN serviciohasatributo sha ON atr.idAtributo = sha.Atributo ' +
        'INNER JOIN carteradeservicios cartera ON cartera.idCdeServicios = sha.Servicio ' +
        'WHERE sha.Servicio = ? ORDER BY atr.Atributo',
        [idCdeServicios]
    );
    res.render('services/editAttr', { lista, attr, cartera:idCdeServicios, nombre: nombre[0] });
});

router.post('/:idCdeServicios/attributes/edit', async (req, res) => {
    const { idCdeServicios } = req.params;
    const { Lista } = req.body;
    const lista = JSON.parse(Lista);

    await pool.query('DELETE FROM serviciohasatributo WHERE Servicio = ?', [idCdeServicios]);
    lista.forEach(async function (el, index) {
        let sha = { 
            Atributo: el.idAtributo,
            Servicio: idCdeServicios
        };
        await pool.query('INSERT INTO serviciohasatributo SET ?', [sha]);
    });
    req.flash("success", "Se han modificado los datos correctamente");
    res.redirect(`/services/${idCdeServicios}/attributes`);
});

module.exports = router;