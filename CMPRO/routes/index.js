const express = require('express');
const router = express.Router();

const pool = require('../databasePool');
const { isLoggedIn } = require('../lib/authentication');
/*  
    Considerar también un método parecido, para saber 
    si el usuario tiene el rol para la acción, como:

    -------------------------
    //en este archivo
    const {hasArchitectPermission} = require('../lib/authentication');

    //en ../lib/authentication
    hasArchitectPermission(req, res, next) {
        if(req.user.tipoUsuario === 'arquitecto'){
            return next();
        }
        return res.redirect('/noPermission/architect');
    } 
    -------------------------
*/

/*---------------Main screen---------------*/
//Agregar parámetros de isLogged y rol
//Dashboard 
router.get('/home', isLoggedIn, async (req, res) => {
    const rol = await pool.query('SELECT Nombre FROM rol WHERE idRol = ?', [req.user.Rol]);
    if ((rol[0].Nombre).toLowerCase() === 'arquitecto') {
        const lastProject = await pool.query(
            'SELECT idProyecto, Proyecto, Fecha_TenFin FROM proyecto WHERE Status=1 ORDER BY idProyecto DESC LIMIT 3'
        );
        const nearProject = await pool.query(
            'SELECT idProyecto, Proyecto, Fecha_TenFin FROM proyecto WHERE Status=1 ORDER BY Fecha_Fin ASC LIMIT 3'
        );
        const lastClientP = await pool.query(
            `SELECT idCliente, RazonSocial, CONCAT(Nombre," ",Apellido) AS persona, Telefono, Correo 
            FROM cliente 
            INNER JOIN usuario ON Usuario = idUsuario 
            ORDER BY idCliente DESC LIMIT 3`
        );
        const lastSupplier = await pool.query(
            `SELECT idProveedor, Nombre 
            FROM proveedor 
            ORDER BY idProveedor DESC LIMIT 3`
        );

        res.render('index/dashboard', { lastProject, nearProject, lastClientP, lastSupplier });
    }
    if ((rol[0].Nombre).toLowerCase() === 'gestor') {
        const lastClient = await pool.query(
            `SELECT idCliente, RazonSocial, CONCAT(Nombre," ",Apellido) AS persona, Telefono, Correo 
            FROM cliente 
            INNER JOIN usuario ON Usuario = idUsuario 
            ORDER BY idCliente ASC LIMIT 3`
        );

        res.render('index/dashboard', { lastClient });
    }
    if ((rol[0].Nombre).toLowerCase() === 'administrador') {
        const lastSupplier = await pool.query(
            `SELECT idProveedor, Nombre 
            FROM proveedor 
            ORDER BY idProveedor DESC LIMIT 3`
        );

        res.render('index/dashboard', { lastSupplier });
    }
    if ((req.user.RolNombre).toLowerCase() === 'cliente'){
        return res.redirect('/client');
    }
    
});

/*---------------Eliminate dashboard´s sections---------------*/
router.get('/dropItems', isLoggedIn, async (req, res) => {
    let classes = [];
    if ((req.user.RolNombre).toLowerCase() === 'arquitecto') {
        classes = [{Clase: 'gestor'},{Clase: 'administrador'}];
    }
    if ((req.user.RolNombre).toLowerCase() === 'gestor') {
        classes = [{Clase: 'arquitecto'}];
    }
    if ((req.user.RolNombre).toLowerCase() === 'administrador') {
        classes = [{Clase: 'gestor'}];
    }

    res.json(classes);
});

module.exports = router;