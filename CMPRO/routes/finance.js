const express = require('express');
const router = express.Router();

const pool = require('../databasePool');

const { isLoggedIn, financeSection, hasArchitectPermission } = require('../lib/authentication');
/*  
    Considerar también un método parecido, para saber 
    si el usuario tiene el rol para la acción, como:

    -------------------------
    //en este archivo
    const {hasArchitectPermission} = require('../lib/authentication');

    //en ../lib/authentication
    hasArchitectPermission(req, res, next) {
        if(req.user.tipoUsuario === 'arquitecto'){
            return next();
        }
        return res.redirect('/architect');
    } 
    -------------------------
*/

/*---------------List financial movements---------------*/
//Corregir consulta, agregar parámetros de isLogged y rol
//Para que el arquitecto pueda ver sus movimientos 
router.get('/:idUsuario', isLoggedIn, hasArchitectPermission, async (req, res) =>{
    const { idUsuario } = req.params;
    const empleado=await pool.query(
        `SELECT idEmpleado FROM empleado
        INNER JOIN usuario ON Usuario=idUsuario
        WHERE idUsuario=?`,[req.user.idUsuario]
    );
    const movements = await pool.query(
        'SELECT idIyE, Tipo, Cantidad, Descripcion, Fecha, CONCAT(nombre," ",apellido) as fullname, Forma ' + 
        'FROM iye ' +
        'INNER JOIN formadepago ON FormaDePago=idFdePago ' +
        'INNER JOIN empleado ON AgregadoPor = idEmpleado '+ 
        'WHERE AgregadoPor =?', [empleado[0].idEmpleado]  
    );
    console.log(movements)

    const arquitecto = await pool.query(
        'SELECT idEmpleado, CONCAT(nombre," ",apellido) as fullname from empleado '
        
    );

    const formaDePago = await pool.query(
        'SELECT idFdePago, Forma ' +
        'FROM formadepago'
    );
    const servicio = await pool.query(
        'SELECT idTiposServicio, Servicio from tiposervicio'
    );
    const proyecto = await pool.query(
        'SELECT idProyecto, Proyecto from proyecto'
    )
    res.render('finance/list', { movements: movements, arquitecto: arquitecto, formaDePago, servicio: servicio, proyecto: proyecto});
});
//List all financial movements
router.get('/', isLoggedIn, financeSection, async (req, res) => {
        const movements = await pool.query(
        'SELECT idIyE, Tipo, Cantidad, Descripcion, Fecha, CONCAT(nombre," ",apellido) as fullname, Forma ' + 
        'FROM iye ' +
        'INNER JOIN formadepago ON FormaDePago=idFdePago '+
        'INNER JOIN empleado ON AgregadoPor = idEmpleado ORDER BY Fecha ' 
        
    );
    console.log(movements)
    const arquitecto = await pool.query(
        'SELECT idEmpleado, CONCAT(nombre," ",apellido) as fullname from empleado '
        
    );

    const formaDePago = await pool.query(
        'SELECT idFdePago, Forma ' +
        'FROM formadepago'
    );
    const servicio = await pool.query(
        'SELECT idTiposServicio, Servicio from tiposervicio'
    );
    const proyecto = await pool.query(
        'SELECT idProyecto, Proyecto from proyecto'
    )
    res.render('finance/list', { movements: movements, arquitecto: arquitecto, formaDePago, servicio: servicio, proyecto: proyecto});
});

/*---------------Add financial movements---------------*/
//Lo que ocupa de la base de datos 
router.get('/add', isLoggedIn, hasArchitectPermission, async(req, res) => {
    const arquitecto = await pool.query(
        'SELECT idEmpleado, CONCAT(nombre," ",apellido) as fullname from empleado'
    );
    const formaDePago = await pool.query(
        'SELECT idFdePago, Forma ' +
        'FROM formadepago'
    );
    const servicio = await pool.query(
        'SELECT idTiposServicio, Servicio from tiposervicio'
    );
    const proyecto = await pool.query(
        'SELECT idProyecto, Proyecto from proyecto'
    )
    res.render('finance/add', {arquitecto: arquitecto, formaDePago: formaDePago, servicio: servicio, proyecto: proyecto});
});


//Agregar a la base de datos 
router.post('/add', async (req, res) => {
    const {TipoMovimiento, Cantidad, Descripcion, TipoServicio, TipoProyecto, Encargado, 
    FormaDePago} = req.body;
    let newFinance = {
        Tipo: TipoMovimiento,
        Cantidad: Cantidad,
        Descripcion: Descripcion, 
        Servicio: TipoServicio, 
        Proyecto: TipoProyecto,
        AgregadoPor: Encargado,
        FormaDePago: FormaDePago
    }; 
    console.log(newFinance);
   const result = await pool.query('INSERT INTO iye SET ?', [newFinance]);
    req.flash("success", "Se ha agregado el movimiento financiero correctamente");
        res.redirect('/finance');
    
});

module.exports = router;