const express = require('express');
const { query } = require('../databasePool');
const router = express.Router();

const pool = require('../databasePool');
const { isLoggedIn, projectsSection } = require('../lib/authentication');

const path = require('path');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: path.join(__dirname, '../public/projects'),
    filename: (req, file, cb) => {
        cb(null, file.originalname)
    }
});



//const {isLoggedIn} = require('../lib/auth');
/*  
    Considerar también un método parecido, para saber 
    si el usuario tiene el rol para la acción, como:

    -------------------------
    //en este archivo
    const {hasArchitectPermission} = require('../lib/authentication');

    //en ../lib/authentication
    hasArchitectPermission(req, res, next) {
        if(req.user.tipoUsuario === 'arquitecto'){
            return next();
        }
        return res.redirect('/architect');
    } 
    -------------------------
*/

/*---------------List projects---------------*/
//Agregar parámetros de isLogged y rol

//List all projects
router.get('/', isLoggedIn, projectsSection, async (req, res) => {
    const projects = await pool.query(
        'SELECT idProyecto, Proyecto, CONCAT(empleado.Nombre," ",empleado.Apellido) as fullname, Fecha_Inicio, Status, RazonSocial ' +
        'FROM proyecto ' +
        'INNER JOIN empleado ON Encargado = idEmpleado ' +
        'INNER JOIN cliente ON Cliente = idCliente'
    );
    const arquitecto = await pool.query(
        'SELECT idEmpleado, CONCAT(nombre," ",apellido) as fullname from empleado'
    );
    const cliente = await pool.query(
        'SELECT idCliente, CONCAT(Nombre," ",Apellido) as fullname from cliente'
    );
    const servicio = await pool.query(
        'SELECT idTiposServicio, Servicio from tiposervicio'
    );
    const estado = await pool.query(
        'SELECT idEstado, Estado from estado'
    );
    const ciudad = await pool.query(
        'SELECT idCiudad, Estado, Ciudad from ciudad'
    );
    res.render('projects/list', { projects: projects, arquitecto: arquitecto, cliente: cliente, servicio: servicio, estado: estado, ciudad: ciudad });
});

//List client projects
router.get('/client/:idCliente', isLoggedIn, projectsSection, async (req, res) => {
    const { idCliente } = req.params;
    const projects = await pool.query(
        'SELECT idProyecto, Proyecto, CONCAT(empleado.Nombre," ",empleado.Apellido) as fullname, Fecha_Inicio, Status, RazonSocial ' +
        'FROM proyecto ' +
        'INNER JOIN empleado ON Encargado = idEmpleado ' +
        'INNER JOIN cliente ON Cliente = idCliente ' +
        'WHERE Cliente = ?', 
        [idCliente]
    );
    
    const cliente = await pool.query(
        `SELECT RazonSocial FROM cliente WHERE idCliente = ?`, 
        [idCliente]
    );
    
    res.render('projects/clientProject', { projects: projects, cliente: cliente[0] });
});

router.get('/show/:idProyecto', isLoggedIn, projectsSection, async (req, res) => {
    const { idProyecto } = req.params;
    const projects = await pool.query(
        'SELECT idProyecto, Proyecto, idCliente, idEmpleado, CONCAT(empleado.Nombre," ", empleado.Apellido) as Encargado, ' +
        'direccion.Colonia, direccion.Calle, direccion.Numero, direccion.CP, direccion.Telefono, estado.Estado, ciudad.Ciudad, ' +
        'Presupuesto, CONCAT(cliente.Nombre," ", cliente.Apellido) as Cliente, ' +
        'tiposervicio.Servicio as TipoServicio, proyecto.STATUS, proyecto.Fecha_TenFin, proyecto.Fecha_Inicio, proyecto.Fecha_Fin, ' +
        'proyecto.Contrato, proyecto.Imagen from proyecto ' +
        'INNER JOIN cliente ON cliente.idCliente = proyecto.Cliente ' +
        'INNER JOIN empleado on empleado.idEmpleado = proyecto.Encargado ' +
        'INNER JOIN direccion on direccion.idDireccion = proyecto.Direccion ' +
        'INNER JOIN ciudad on ciudad.idCiudad = direccion.Ciudad ' +
        'INNER JOIN estado on estado.idEstado = ciudad.Estado ' +
        'INNER JOIN tiposervicio on tiposervicio.idTiposServicio = proyecto.TipoServicio ' +
        'WHERE idProyecto = ?', [idProyecto]
    );

    res.render('projects/show', { projects: projects[0] });
});

router.get('/add', isLoggedIn, projectsSection, async (req, res) => {
    const arquitecto = await pool.query(
        'SELECT idEmpleado, CONCAT(nombre," ",apellido) as fullname from empleado'
    );
    const cliente = await pool.query(
        'SELECT idCliente, CONCAT(Nombre," ",Apellido) as fullname from cliente'
    );
    const servicio = await pool.query(
        'SELECT idTiposServicio, Servicio from tiposervicio'
    );
    const estado = await pool.query(
        'SELECT idEstado, Estado from estado'
    );
    const ciudad = await pool.query(
        'SELECT idCiudad, Estado, Ciudad from ciudad'
    );

    res.render('projects/add', { arquitecto: arquitecto, cliente: cliente, servicio: servicio, estado: estado, ciudad: ciudad });
});



/*---------------Add project---------------*/
const upload = multer({
    storage: storage,
    dest: path.join(__dirname, '../public/projects'),
    limits: { fileSize: 2000000 }
}).fields([{ name: 'Contrato' }, { name: 'Imagen' }]);

router.post('/add', upload, async (req, res) => {
    const {
        Proyecto,
        Encargado,
        Cliente,
        TipoServicio,
        Presupuesto,
        Contrato,
        Imagen,
        Fecha_TenFin,
        Status,
        Estado,
        Ciudad,
        Colonia,
        Calle,
        Numero,
        CP,
        Telefono
    } = req.body;
    let newDireccion = {
        Ciudad,
        Colonia,
        Calle,
        Numero,
        CP,
        Telefono
    };

    const result2 = await pool.query('INSERT INTO direccion set ?', [newDireccion]);

    var files = [];
    var fileKeys = Object.keys(req.files);
    fileKeys.forEach(function (key) {
        files.push(req.files[key]);
    });

    if (files[0] == null && files[1] == null) {
        let newProyecto = {
            Proyecto,
            Encargado,
            Cliente,
            TipoServicio,
            Presupuesto,
            Fecha_TenFin,
            Status: '1'
        };
        newProyecto.Direccion = result2.insertId;

        await pool.query('INSERT INTO proyecto set ?', [newProyecto]);

        req.flash("success", "Se ha agregado el proyecto correctamente");
        res.redirect('/projects');

    } else if (files[0] != null && files[1] != null) {

        var filename0 = files[0][0].originalname;
        var filename1 = files[1][0].originalname;

        let newProyecto = {
            Proyecto,
            Encargado,
            Cliente,
            TipoServicio,
            Presupuesto,
            Contrato: `/projects/${filename0}`,
            Imagen: `/projects/${filename1}`,
            Fecha_TenFin,
            Status: '1'
        };
        newProyecto.Direccion = result2.insertId;
        await pool.query('INSERT INTO proyecto set ?', [newProyecto]);

        req.flash("success", "Se ha agregado el proyecto correctamente");
        res.redirect('/projects');
    } else if (files[0][0].fieldname == 'Imagen') {
        var filename1 = files[0][0].originalname;
        let newProyecto = {
            Proyecto,
            Encargado,
            Cliente,
            TipoServicio,
            Presupuesto,
            Imagen: `/projects/${filename1}`,
            Fecha_TenFin,
            Status: '1'
        };
        newProyecto.Direccion = result2.insertId;
        await pool.query('INSERT INTO proyecto set ?', [newProyecto]);

        req.flash("success", "Se ha agregado el proyecto correctamente");
        res.redirect('/projects');
    } else if (files[0][0].fieldname == 'Contrato') {
        var filename0 = files[0][0].originalname;
        let newProyecto = {
            Proyecto,
            Encargado,
            Cliente,
            TipoServicio,
            Presupuesto,
            Contrato: `/projects/${filename0}`,
            Fecha_TenFin,
            Status: '1'
        };
        newProyecto.Direccion = result2.insertId;
        await pool.query('INSERT INTO proyecto set ?', [newProyecto]);

        req.flash("success", "Se ha agregado el proyecto correctamente");
        res.redirect('/projects');
    }

});

router.get('/edit/:idProyecto', isLoggedIn, projectsSection, async (req, res) => {
    const { idProyecto } = req.params;
    const projects = await pool.query(
        'SELECT idProyecto, Proyecto, idCliente, idEmpleado, idDireccion, idTiposServicio, CONCAT(empleado.Nombre," ", empleado.Apellido) as Encargado, ' +
        'direccion.Colonia, direccion.Calle, direccion.Numero, direccion.CP, direccion.Telefono, estado.Estado, estado.idEstado, ciudad.Ciudad, ciudad.idCiudad, ' +
        'Presupuesto, CONCAT(cliente.Nombre," ", cliente.Apellido) as Cliente, ' +
        'tiposervicio.Servicio as TipoServicio, proyecto.Status, proyecto.Fecha_TenFin, proyecto.Fecha_Fin, ' +
        'proyecto.Contrato, proyecto.Imagen from proyecto ' +
        'INNER JOIN cliente ON cliente.idCliente = proyecto.Cliente ' +
        'INNER JOIN empleado on empleado.idEmpleado = proyecto.Encargado ' +
        'INNER JOIN direccion on direccion.idDireccion = proyecto.Direccion ' +
        'INNER JOIN ciudad on ciudad.idCiudad = direccion.Ciudad ' +
        'INNER JOIN estado on estado.idEstado = ciudad.Estado ' +
        'INNER JOIN tiposervicio on tiposervicio.idTiposServicio = proyecto.TipoServicio ' +
        'WHERE idProyecto = ?', [idProyecto]
    );
    console.log(projects[0].Fecha_Fin)
    const arquitecto = await pool.query(
        'SELECT idEmpleado, CONCAT(nombre," ",apellido) as fullname from empleado'
    );
    const cliente = await pool.query(
        'SELECT idCliente, CONCAT(Nombre," ",Apellido) as fullname from cliente'
    );
    const servicio = await pool.query(
        'SELECT idTiposServicio, Servicio from tiposervicio'
    );

    const estado = await pool.query(
        'SELECT idEstado, Estado from estado'
    );
    const ciudad = await pool.query(
        'SELECT idCiudad, Ciudad from ciudad'
    );

    res.render('projects/edit', { projects: projects[0], arquitecto: arquitecto, cliente: cliente, servicio: servicio, estado: estado, ciudad: ciudad });
});



router.post('/edit/:idProyecto', upload, async (req, res) => {
    const { idProyecto } = req.params;

    const {
        Proyecto,
        Encargado,
        Cliente,
        TipoServicio,
        Presupuesto,
        Contrato,
        Imagen,
        Fecha_TenFin,
        Status,
        Fecha_Fin,
        Estado,
        Ciudad,
        Colonia,
        Calle,
        Numero,
        CP,
        Telefono
    } = req.body;


    var files = [];
    var fileKeys = Object.keys(req.files);
    fileKeys.forEach(function (key) {
        files.push(req.files[key]);
    });

    if (files[0] == null && files[1] == null) {
        let newProyecto = {
            Proyecto,
            Encargado,
            Cliente,
            TipoServicio,
            Presupuesto,
            Fecha_TenFin,
            Status
        };
        await pool.query('UPDATE proyecto set ? where idProyecto = ?', [newProyecto, idProyecto]);

    } else if (files[0] != null && files[1] != null) {

        var filename0 = files[0][0].originalname;
        var filename1 = files[1][0].originalname;

        let newProyecto = {
            Proyecto,
            Encargado,
            Cliente,
            TipoServicio,
            Presupuesto,
            Contrato: `/projects/${filename0}`,
            Imagen: `/projects/${filename1}`,
            Fecha_TenFin,
            Status
        };
        await pool.query('UPDATE proyecto set ? where idProyecto = ?', [newProyecto, idProyecto]);

    } else if (files[0][0].fieldname == 'Imagen') {
        var filename1 = files[0][0].originalname;
        let newProyecto = {
            Proyecto,
            Encargado,
            Cliente,
            TipoServicio,
            Presupuesto,
            Imagen: `/projects/${filename1}`,
            Fecha_TenFin,
            Status
        };
        await pool.query('UPDATE proyecto set ? where idProyecto = ?', [newProyecto, idProyecto]);
    } else if (files[0][0].fieldname == 'Contrato') {
        var filename0 = files[0][0].originalname;
        let newProyecto = {
            Proyecto,
            Encargado,
            Cliente,
            TipoServicio,
            Presupuesto,
            Contrato: `/projects/${filename0}`,
            Fecha_TenFin,
            Status
        };
        await pool.query('UPDATE proyecto set ? where idProyecto = ?', [newProyecto, idProyecto]);
    }

    const idDireccion = await pool.query('SELECT direccion.idDireccion FROM direccion ' +
        'INNER JOIN proyecto ON proyecto.Direccion = direccion.idDireccion ' +
        'WHERE proyecto.idProyecto = ?', [idProyecto]);
    console.log(idDireccion);

    const newDireccion = {
        Ciudad,
        Colonia,
        Calle,
        Numero,
        CP,
        Telefono
    };

    await pool.query('UPDATE direccion SET ? WHERE idDireccion = ?', [newDireccion, idDireccion[0].idDireccion]);
    console.log(req.files);
    console.log(filename0);
    req.flash("success", "Se han editado los datos correctamente");
    res.redirect('/projects');

});

router.get('/ciudad/:idEstado', async (req, res) => {
    const { idEstado } = req.params;
    const ciudades = await pool.query(
        'SELECT idCiudad, Ciudad from ciudad where Estado = ?', [idEstado]
    );
    res.json(ciudades);
});

/*---------------List project´s quotes---------------*/
//List all quotes
router.get('/:idProyecto/quotes', isLoggedIn, projectsSection, async (req, res) => {
    const { idProyecto } = req.params;

    const suma=await pool.query(
        'SELECT SUM(Costo) as total FROM cotizacion WHERE Proyecto=?',
        [idProyecto]
    );

    const quotes = await pool.query(
        `SELECT idCotizacion, Proyecto, cds.Servicio, qu.Costo, qu.Descripcion 
        FROM cotizacion qu 
        INNER JOIN carteradeservicios cds ON qu.Servicio = cds.idCdeServicios 
        WHERE qu.Proyecto = ?`,
        [idProyecto]
    );
    const services = await pool.query(
        'SELECT idCdeServicios, tipo.Servicio AS tipos, cartera.Servicio AS nombre, Costo, Descripcion ' +
        'FROM carteradeservicios cartera ' +
        'INNER JOIN tiposervicio tipo ON cartera.TiposServicio = tipo.idTiposServicio ' +
        // 'WHERE idCdeServicios > 10 ' +
        'ORDER BY idCdeServicios ASC'
    );

    res.render('projects/listQuote', { quotes, services, proyecto: idProyecto, suma });
});

/*---------------Add quote---------------*/
router.post('/:idProyecto/quotes/add', async (req, res) => {
    const { idProyecto } = req.params;
    const { Servicio, Costo, Descripcion } = req.body;

    const attr = await pool.query(
        `SELECT sha.Atributo AS attr, at.Atributo AS nom, '0' AS stat 
        FROM serviciohasatributo sha 
        INNER JOIN atributo at ON sha.Atributo = at.idAtributo 
        WHERE Servicio = ?`,
        [Servicio]
    );
    console.log(JSON.stringify(attr))
    let quote = {
        Proyecto: idProyecto,
        Servicio,
        Costo,
        Checklist: JSON.stringify(attr),
        Descripcion
    };
    await pool.query('INSERT INTO cotizacion SET ?', [quote])

    req.flash("success", "Se ha agregado la cotización al proyecto");
    res.redirect(`/projects/${idProyecto}/quotes`);
});

/*---------------Edit project´s quotes---------------*/
//List quote´s info
router.get('/:idCotizacion/checklist', async (req, res) => {
    const { idCotizacion } = req.params;
    const quote = await pool.query(
        'SELECT Costo, Descripcion, Checklist FROM cotizacion WHERE idCotizacion = ?',
        [idCotizacion]
    );
    let attr = JSON.parse(quote[0].Checklist);

    let response = {
        cotizacion: quote[0],
        atributos: attr
    };
    console.log(response.cotizacion)

    // const attr = [];
    // await aux.forEach(async function (el, index) {
    //     let atributo = await pool.query(
    //         'SELECT Atributo FROM atributo WHERE idAtributo = ?',
    //         [el.attr]
    //     );
    //     //console.log(atributo)
    //     let obj = new Object();
    //     obj.attr = el.attr;
    //     obj.stat = el.stat;
    //     obj.nombre = atributo[0].Atributo;
    //     console.log(obj)
    //     attr.push(obj);
    // });
    // await console.log(quote)
    // await console.log(JSON.stringify(attr))

    res.json(response);
});

//Edit quote´s info
router.post('/:idProyecto/quotes/edit', async (req, res) => {
    const { idProyecto } = req.params;
    const { Costo, Descripcion, Atributos, idCotizacion } = req.body;
    
    let quote = {
        Costo,
        Checklist: Atributos,
        Descripcion
    };
    await pool.query('UPDATE cotizacion SET ? WHERE idCotizacion = ?', [quote, idCotizacion]);

    req.flash("success", "Se ha editado la cotización del proyecto");
    res.redirect(`/projects/${idProyecto}/quotes`);
});router.post('/:idProyecto/quotes/edit', async (req, res) => {
    const { idProyecto } = req.params;
    const { Costo, Descripcion, Atributos, idCotizacion } = req.body;

    let quote = {
        Costo,
        Checklist: Atributos,
        Descripcion
    };
    await pool.query('UPDATE cotizacion SET ? WHERE idCotizacion = ?', [quote, idCotizacion]);

    req.flash("success", "Se ha editado la cotización del proyecto");
    res.redirect(`/projects/${idProyecto}/quotes`);
});

router.get('/:Proyecto/quotes/delete/:idCotizacion', async (req, res) => {
    const { Proyecto } = req.params;
    const { idCotizacion } = req.params;
    await pool.query('DELETE FROM cotizacion WHERE idCotizacion = ?', [idCotizacion]);

    req.flash("success", "Se ha eliminado la cotización del proyecto");
    res.redirect(`/projects/${Proyecto}/quotes`);
});

module.exports = router;