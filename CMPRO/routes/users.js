const express = require('express');
const router = express.Router();

const pool = require('../databasePool');
const helpers = require('../lib/helpers');
const { isLoggedIn, usersSection } = require('../lib/authentication');

const path = require('path');
const multer = require('multer');

const fs = require('fs');
const { v4: uuidv4 } = require('uuid')
let ruta = ''

const storage = multer.diskStorage({
    destination: path.join(__dirname, '../public/img/employee'),
    filename: (req, file, cb) => {
        ruta = uuidv4() + path.extname(file.originalname).toLocaleLowerCase();
        cb(null, ruta);
    }
});
/*  
    Considerar también un método parecido, para saber 
    si el usuario tiene el rol para la acción, como:

    -------------------------
    //en este archivo
    const {hasArchitectPermission} = require('../lib/authentication');

    //en ../lib/authentication
    hasArchitectPermission(req, res, next) {
        if(req.user.tipoUsuario === 'arquitecto'){
            return next();
        }
        return res.redirect('/noPermission/architect');
    } 
    -------------------------
*/

/*---------------List users---------------*/
//Agregar parámetros de isLogged y rol
//List all users
router.get('/', isLoggedIn, usersSection, async (req, res) => {
    const roles = await pool.query(
        'SELECT idRol, Nombre FROM rol WHERE Nombre <> ?',
        ['Cliente']
    );
    const users = await pool.query(
        'SELECT idEmpleado, empleado.Nombre, Apellido, Correo, rol.Nombre as Cargo, RFC, Imagen ' +
        'FROM empleado ' +
        'INNER JOIN usuario ON idUsuario = Usuario ' +
        'INNER JOIN rol ON idRol = usuario.Rol'
    );
    
    res.render('users/list', { users: users, roles });
});

router.get('/role/:idUsuario', async (req, res) => {
    const { idUsuario } = req.params;
    const rol = await pool.query(
        'SELECT Rol FROM usuario INNER JOIN empleado ON idUsuario=Usuario WHERE idEmpleado = ?',
        [idUsuario]
    );
    res.json(rol);
});

/*---------------Add user---------------*/
router.get('/add', isLoggedIn, usersSection, async (req, res) => {
    const roles = await pool.query('SELECT idRol, Nombre FROM rol WHERE Nombre <> ?', ['Cliente']);
    res.render('users/add', { roles: roles });
});

router.post('/add', async (req, res) => {
    const { Nombre, Apellido, Rol, Contrasena, Correo, RFC } = req.body;
    let usuario = { Rol, Correo, Contrasena };
    usuario.Contrasena = await helpers.encryptPassword(Contrasena);
    const result = await pool.query(
        'INSERT INTO usuario SET ?', [usuario]
    );
    let empleado = { Nombre, Apellido, RFC };
    empleado.Usuario = result.insertId;
    empleado.Imagen = "";
    const result2 = await pool.query(
        'INSERT INTO empleado SET ?', [empleado]
    )
    req.flash("success", "Se ha agregado al usuario correctamente");
    res.redirect("/users");
});

/*---------------Edit user---------------*/
router.get('/edit/:idEmpleado', isLoggedIn, usersSection, async (req, res) => {
    const { idEmpleado } = req.params;
    const roles = await pool.query(
        'SELECT idRol, Nombre ' +
        'FROM rol ' +
        'WHERE Nombre <> ?', ['Cliente']
    );
    const empleado = await pool.query(
        'SELECT idEmpleado, Nombre, Apellido, RFC, usuario.Rol, Correo ' +
        'FROM empleado ' +
        'INNER JOIN usuario ON Usuario = idUsuario ' +
        'WHERE idEmpleado = ?', [idEmpleado]
    );
    res.render('users/edit', { empleado: empleado[0], roles: roles });
})

router.post('/edit', async (req, res) => {
    const { idEmpleado } = req.body;
    const rows = await pool.query(
        'SELECT Usuario ' +
        'FROM empleado ' +
        'WHERE idEmpleado = ? ', [idEmpleado]
    );
    if (rows.length > 0) {
        const empleado = rows[0];
        const { Rol, Correo } = req.body;
        let usuario = { Correo, Rol };
        const result = await pool.query(
            'UPDATE usuario SET ? ' +
            'WHERE idUsuario = ? ', [usuario, empleado.Usuario]
        );
        req.flash("success", "Se ha modificado al usuario correctamente");
        res.redirect("/users");
    }
});

router.post('/edit', async (req, res) => {
    const { idEmpleado } = req.body;
    const rows = await pool.query(
        'SELECT Usuario ' +
        'FROM empleado ' +
        'WHERE idEmpleado = ? ', [idEmpleado]
    );
    if (rows.length > 0) {
        const empleado = rows[0];
        const { Rol, Correo } = req.body;
        let usuario = { Correo, Rol };
        const result = await pool.query(
            'UPDATE usuario SET ? ' +
            'WHERE idUsuario = ? ', [usuario, empleado.Usuario]
        );
        req.flash("success", "Se ha modificado al usuario correctamente");
        res.redirect("/users");
    }
});

//Edit user photo
router.get('/image/:idEmpleado', async (req, res) => {
    const { idEmpleado } = req.params;
    const imagen = await pool.query(
        'SELECT Imagen FROM empleado WHERE idEmpleado = ?',
        [idEmpleado]
    );
    res.json(imagen);
});

const photo = multer({
    storage: storage,
    dest: path.join(__dirname, '../public/img/employee'),
    limits: { fileSize: 2000000 },
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/;
        const mimetype = fileTypes.test(file.mimetype);
        const extName = fileTypes.test(path.extname(file.originalname));
        if (mimetype && extName) {
            return cb(null, true);
        }
        cb('Error: File must be an image');
    }
}).single('empleado-foto');

router.post('/photo', photo, async (req, res) => {
    const { idEmpleado } = req.body;
    //const rows = await pool.query('SELECT idEmpleado FROM empleado WHERE Usuario = ?', [req.user.idUsuario]);
    //if (rows.length > 0) {
    //const empleado = rows[0];
    const rows = await pool.query('SELECT Imagen FROM empleado WHERE idEmpleado = ?', [idEmpleado]);
    let imagen = rows[0].Imagen;
    if (imagen !== null && imagen !== '') {
        var filePath = path.join(__dirname, `../public${imagen}`);
        fs.unlinkSync(filePath);
    }

    let Imagen = `/img/employee/${ruta}`;

    await pool.query('UPDATE empleado SET Imagen = ? WHERE idEmpleado = ?', [Imagen, idEmpleado]);
    req.flash("success", "Se ha modificado la foto correctamente");
    res.redirect('/users');
    //}
});

module.exports = router;