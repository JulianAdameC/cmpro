const express = require('express');
const router = express.Router();

const passport = require('passport');

const pool = require('../databasePool');
const { isLoggedIn, hasClientPermission } = require('../lib/authentication');

//List all client projects
router.get('/', isLoggedIn, hasClientPermission, async(req, res) => {
    const client = await pool.query(
        'SELECT idCliente '+
        'FROM cliente '+
        'INNER JOIN usuario ON Usuario = idUsuario '+
        'WHERE Usuario = ?', [req.user.idUsuario]
    );

    const projects = await pool.query(
        'SELECT idProyecto, Proyecto, CONCAT(nombre," ",apellido) as fullname, Fecha_Inicio, Status ' +
        'FROM proyecto ' +
        'INNER JOIN empleado ON Encargado = idEmpleado ' +
        'WHERE Cliente= ?', [client[0].idCliente]
    );
    res.render('clientProfile/list', { projects: projects });
});

//List all quotes
router.get('/:idProyecto/quotes', isLoggedIn, hasClientPermission, async (req, res) => {
    const { idProyecto } = req.params;
    const quotes = await pool.query(
        `SELECT idCotizacion, cds.Servicio, qu.Costo, qu.Descripcion 
        FROM cotizacion qu 
        INNER JOIN carteradeservicios cds ON qu.Servicio = cds.idCdeServicios 
        WHERE qu.Proyecto = ?`,
        [idProyecto]
    );

    res.render('clientProfile/listQuote', { quotes });
});

/*---------------Edit password---------------*/
router.get('/password', isLoggedIn, hasClientPermission, (req, res) => {
    res.render('clientProfile/editPassword');
});

router.post('/password', (req, res, next) => {
    passport.authenticate('client.editPassword', {
        successRedirect: '/client',
        failureRedirect: '/client/password',
        failureFlash: true
    })(req, res, next);
});

module.exports=router;