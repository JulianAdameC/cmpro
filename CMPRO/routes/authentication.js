const express = require('express');
const router = express.Router();

const passport = require('passport');
const helpers = require('../lib/helpers');
const { isLoggedIn, isNotLoggedIn } = require('../lib/authentication');

const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const pool = require('../databasePool');

/*---------------Sign In empleado---------------*/
router.get('/authenticate', isNotLoggedIn, (req, res) => {
    res.render('authentication/login');
});

router.post('/authenticate', (req, res, next) => {
    passport.authenticate('local.signin', {
        successRedirect: '/home',
        failureRedirect: '/authenticate',
        failureFlash: true
    })(req, res, next);
});

/*---------------Sign In cliente---------------*/
router.post('/authenticateClient', (req, res, next) => {
    passport.authenticate('local.signinClient', {
        successRedirect: '/client',
        failureRedirect: '/authenticate',
        failureFlash: true
    })(req, res, next);
});

/*---------------Recover password empleado---------------*/
router.get('/forgotPassword', isNotLoggedIn, (req, res) => {
    res.render('authentication/recover');
});

router.post('/forgotPassword', async (req, res, next) => {
    const { email } = req.body;
    try {
        const rows = await pool.query(
            'SELECT idUsuario, Correo, CONCAT(Nombre," ",Apellido) as fullname ' +
            'FROM usuario ' +
            'INNER JOIN empleado ON idUsuario = Usuario ' +
            'WHERE Correo = ?',
            [email]
        );
        if (rows.length > 0) {
            const user = rows[0];
            const token = jwt.sign(
                { id: user.idUsuario, Correo: user.Correo },
                'secretRecoverToken',
                { expiresIn: 60 * 15 }
            );

            let emailContent = `
                <img src='cid:logo@example.com' width="200">
                <h2>Sistema de gestión CM-Pro</h4>
                <h4>Hola, ${user.fullname}</h4>
                <h6>Has solicitado un mensaje para recuperar tu acceso al sistema.</h6>
                <h6>Utiliza el siguiente enlace para continuar con la recuperación de tu cuenta</h6>
                <a href='http://localhost:4000/resetPassword/${token}'>Link de recuperación</a><br>
                <h7>Hecho por CodeNeo Development®</h7>
                <img src='cid:codeneo@example.com' width="200">
            `;

            const transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 465,
                secure: true,
                auth: {
                    user: 'codeneo.dev@gmail.com',
                    pass: 'qrqmernzokpyxliu'
                }
            });

            let info = await transporter.sendMail({
                from: '"CodeNeo Development 👻" <codeneo.dev@gmail.com>', // sender address
                to: user.Correo, // list of receivers
                subject: "Recuperación de contraseña ✔", // Subject line
                html: emailContent, // html body
                attachments: [
                    {
                        filename: 'ConsultArq ✔.png',
                        path: 'http://localhost:4000/img/Consultarq_letras_azul.png',
                        cid: 'logo@example.com' // should be as unique as possible
                    },
                    {
                        filename: 'CodeNeo ✔.jpg',
                        path: 'http://localhost:4000/img/codeneo.jpg',
                        cid: 'codeneo@example.com' // should be as unique as possible
                    }
                ]
            });
        }
    } catch (error) {
        console.log('Couldn´t send email');
    }
    finally {
        req.flash('success', 'Se ha enviado un enlace de recuperación a tu correo');
        res.redirect('/authenticate');
    }
});

router.get('/resetPassword/:token', isNotLoggedIn, (req, res) => {
    try {
        const { token } = req.params;
        const decoded = jwt.verify(token, 'secretRecoverToken');
        res.render('authentication/reset', { token: token });
    } catch (error) {
        req.flash('error', 'El enlace no está disponible. Solicita otro mensaje para recuperar tu contraseña');
        res.redirect('/authenticate');
    }
});

router.post('/resetPassword/:token', async (req, res) => {
    try {
        const { token } = req.params;
        const decoded = jwt.verify(token, 'secretRecoverToken');

        const rows = await pool.query('SELECT idUsuario FROM usuario WHERE Correo = ?', [decoded.Correo]);
        if (rows.length > 0) {
            const user = rows[0];
            let { Contrasena } = req.body;
            Contrasena = await helpers.encryptPassword(Contrasena);
            await pool.query('UPDATE usuario SET Contrasena = ? WHERE idUsuario = ?', [Contrasena, user.idUsuario]);
            req.flash('success', 'Se ha modificado tu contraseña');
            // res.redirect('/authenticate');
        }
    } catch (error) {
        console.log(error)
        req.flash('error', 'No se pudo cambiar la contraseña.');
        // res.redirect('/authenticate');
    }
    finally {
        res.redirect('/authenticate');
    }
});

/*---------------Recover password cliente---------------*/
router.get('/forgotPasswordClient', isNotLoggedIn, (req, res) => {
    res.render('authentication/recoverClient');
});

router.post('/forgotPasswordClient', async (req, res, next) => {
    const { email } = req.body;
    try {
        const rows = await pool.query(
            'SELECT idUsuario, Correo, CONCAT(Nombre," ",Apellido) as fullname ' +
            'FROM usuario ' +
            'INNER JOIN cliente ON idUsuario = Usuario ' +
            'WHERE Correo = ?',
            [email]
        );
        if (rows.length > 0) {
            const user = rows[0];
            const token = jwt.sign(
                { id: user.idUsuario, Correo: user.Correo },
                'secretRecoverToken',
                { expiresIn: 60 * 15 }
            );

            let emailContent = `
                <img src='cid:logo@example.com' width="200">
                <h2>Sistema de gestión CM-Pro</h4>
                <h4>Hola, ${user.fullname}</h4>
                <h6>Has solicitado un mensaje para recuperar tu acceso al sistema.</h6>
                <h6>Utiliza el siguiente enlace para continuar con la recuperación de tu cuenta</h6>
                <a href='http://localhost:4000/resetPassword/${token}'>Link de recuperación</a><br>
                <h7>Hecho por CodeNeo Development®</h7>
                <img src='cid:codeneo@example.com' width="200">
            `;

            const transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 465,
                secure: true,
                auth: {
                    user: 'codeneo.dev@gmail.com',
                    pass: 'qrqmernzokpyxliu'
                }
            });

            let info = await transporter.sendMail({
                from: '"CodeNeo Development 👻" <codeneo.dev@gmail.com>', // sender address
                to: user.Correo, // list of receivers
                subject: "Recuperación de contraseña ✔", // Subject line
                html: emailContent, // html body
                attachments: [
                    {
                        filename: 'ConsultArq ✔.png',
                        path: 'http://localhost:4000/img/Consultarq_letras_azul.png',
                        cid: 'logo@example.com' // should be as unique as possible
                    },
                    {
                        filename: 'CodeNeo ✔.jpg',
                        path: 'http://localhost:4000/img/codeneo.jpg',
                        cid: 'codeneo@example.com' // should be as unique as possible
                    }
                ]
            });
        }
    } catch (error) {
        console.log('Couldn´t send email');
    }
    finally {
        req.flash('success', 'Se ha enviado un enlace de recuperación a tu correo');
        res.redirect('/authenticate');
    }
});

router.get('/resetPasswordClient/:token', isNotLoggedIn, (req, res) => {
    try {
        const { token } = req.params;
        const decoded = jwt.verify(token, 'secretRecoverToken');
        res.render('authentication/resetClient', { token: token });
    } catch (error) {
        req.flash('error', 'El enlace no está disponible. Solicita otro mensaje para recuperar tu contraseña');
        res.redirect('/authenticate');
    }
});

router.post('/resetPasswordClient/:token', async (req, res) => {
    try {
        const { token } = req.params;
        const decoded = jwt.verify(token, 'secretRecoverToken');

        const rows = await pool.query('SELECT idUsuario FROM usuario WHERE Correo = ?', [decoded.Correo]);
        if (rows.length > 0) {
            const user = rows[0];
            let { Contrasena } = req.body;
            Contrasena = await helpers.encryptPassword(Contrasena);
            await pool.query('UPDATE usuario SET Contrasena = ? WHERE idUsuario = ?', [Contrasena, user.idUsuario]);
            req.flash('success', 'Se ha modificado tu contraseña');
            // res.redirect('/authenticate');
        }
    } catch (error) {
        console.log(error)
        req.flash('error', 'No se pudo cambiar la contraseña.');
        // res.redirect('/authenticate');
    }
    finally {
        res.redirect('/authenticate');
    }
});

/*---------------Sign Up---------------*/
/*//Modificar para agregar usuarios
router.get('/signup', (req, res) => {
    res.render('auth/signup');
});

router.post('/signup', passport.authenticate('local.signup', {
    successRedirect: '/profile', 
    failureRedirect: '/signup', 
    failureFlash: true
}));*/

/*---------------Log out---------------*/
router.get('/logout', (req, res) => {
    req.logOut();
    res.redirect('/authenticate');
});


/*---------------Eliminate sidebar elements---------------*/
router.get('/dropElements', isLoggedIn, async (req, res) => {
    let classes = [];
    if ((req.user.RolNombre).toLowerCase() === 'arquitecto') {
        classes = [{Clase: 'service'},{Clase: 'user'},{Clase: 'log'},{Clase: 'other'}];
        //classes = '.service.user.log';
    }
    if ((req.user.RolNombre).toLowerCase() === 'gestor') {
        classes = [{Clase: 'project'},{Clase: 'finance'},{Clase: 'supplier'}];
        //classes = '.project.finance.supplier.service';
    }
    if ((req.user.RolNombre).toLowerCase() === 'administrador') {
        classes = [{Clase: 'client'},{Clase: 'project'},{Clase: 'service'},{Clase: 'user'},{Clase: 'log'},{Clase: 'other'}];
        //classes = '.client.project.user.log';
    }
    if ((req.user.RolNombre).toLowerCase() === 'cliente') {
        classes = [{Clase:'profileinfo'},{Clase: 'client'},{Clase: 'project'},{Clase: 'finance'},{Clase: 'supplier'},{Clase: 'service'},{Clase: 'user'},{Clase: 'log'},{Clase: 'other'}];
        //classes = '.client.project.finance.supplier.service.user.log';
    }

    res.json(classes);
});

/*---------------Disable supplier´s elements---------------*/
router.get('/disableSupplier', isLoggedIn, async (req, res) => {
    let classes = [];
    if ((req.user.RolNombre).toLowerCase() === 'arquitecto') {
        classes = [{Clase: 'gestor'}];
    }

    res.json(classes);
});

module.exports = router;