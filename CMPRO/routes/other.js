const express = require('express');
const router = express.Router();

const pool = require('../databasePool');
const { isLoggedIn, hasGestorPermission } = require('../lib/authentication');
/*  
    Considerar también un método parecido, para saber 
    si el usuario tiene el rol para la acción, como:

    -------------------------
    //en este archivo
    const {hasArchitectPermission} = require('../lib/authentication');

    //en ../lib/authentication
    hasArchitectPermission(req, res, next) {
        if(req.user.tipoUsuario === 'arquitecto'){
            return next();
        }
        return res.redirect('/noPermission/architect');
    } 
    -------------------------
*/

/*---------------List states---------------*/
//Agregar parámetros de isLogged y rol
//List all states
router.get('/states', isLoggedIn, hasGestorPermission, async (req, res) => {
    const states = await pool.query(
        'SELECT idEstado, Estado ' +
        'FROM estado' 
    );
    res.render('other/state', { states: states });
});

/*---------------Add state---------------*/
// router.get('/state', (req, res) => {
//     res.render('clients/add');
// });

router.post('/states/add', async (req, res) => {
    const { Add_Estado } = req.body;
    let estado = {
        Estado: Add_Estado
    };

    const result = await pool.query('INSERT INTO estado SET ?', [estado]);
    req.flash("success", "Se ha agregado el estado correctamente");
    res.redirect("/other/states");
});

/*---------------Edit state---------------*/
// router.get('/edit/:idCliente', async(req, res) => {
//     const { idCliente } = req.params;
//     const cliente = await pool.query(
//         'SELECT idCliente, Nombre, Apellido, Telefono, RFC, RazonSocial, Correo ' +
//         'FROM cliente ' +
//         'INNER JOIN usuario ON cliente.Usuario = idUsuario ' +
//         'WHERE idCliente = ?',
//         [idCliente]
//     );
//     res.render('clients/edit', { cliente: cliente[0] });
// });

router.post('/states/edit', async (req, res) => {
    const { Edit_Estado, Edit_ID } = req.body;
    let estado = {
        Estado: Edit_Estado
    };

    const result = await pool.query('UPDATE estado SET ? WHERE idEstado = ?', [estado, Edit_ID]);
    req.flash("success", "Se ha editado el estado correctamente");
    res.redirect("/other/states");
});


/*---------------List cities---------------*/
//Agregar parámetros de isLogged y rol
//List all cities
router.get('/states/:idEstado/cities', isLoggedIn, hasGestorPermission, async (req, res) => {
    const { idEstado } = req.params;
    const estado = await pool.query('SELECT * FROM estado WHERE idEstado = ?', [idEstado]);
    console.log(estado[0])
    const cities = await pool.query(
        'SELECT idCiudad, Ciudad ' +
        'FROM ciudad WHERE Estado = ?',
        [estado[0].idEstado] 
    );
    console.log(cities)
    res.render('other/city', { cities, estado: estado[0] });
});

/*---------------Add city---------------*/
// router.get('/add', (req, res) => {
//     res.render('clients/add');
// });

router.post('/states/:idEstado/cities/add', async (req, res) => {
    const { idEstado } = req.params;
    const { Add_Ciudad } = req.body;
    let ciudad = {
        Ciudad: Add_Ciudad,
        Estado: idEstado
    };

    const result = await pool.query('INSERT INTO ciudad SET ?', [ciudad]);
    req.flash("success", "Se ha agregado la ciudad correctamente");
    res.redirect(`/other/states/${idEstado}/cities`);
});

/*---------------Edit city---------------*/
// router.get('/edit/:idCliente', async(req, res) => {
//     const { idCliente } = req.params;
//     const cliente = await pool.query(
//         'SELECT idCliente, Nombre, Apellido, Telefono, RFC, RazonSocial, Correo ' +
//         'FROM cliente ' +
//         'INNER JOIN usuario ON cliente.Usuario = idUsuario ' +
//         'WHERE idCliente = ?',
//         [idCliente]
//     );
//     res.render('clients/edit', { cliente: cliente[0] });
// });

router.post('/states/:idEstado/cities/edit', async (req, res) => {
    const { idEstado } = req.params;
    const { Edit_Ciudad, Edit_ID } = req.body;
    let ciudad = {
        Ciudad: Edit_Ciudad
    };

    const result = await pool.query('UPDATE ciudad SET ? WHERE idCiudad = ?', [ciudad, Edit_ID]);
    req.flash("success", "Se ha editado la ciudad correctamente");
    res.redirect(`/other/states/${idEstado}/cities`);
});

module.exports = router;