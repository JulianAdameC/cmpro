const express = require('express');
const router = express.Router();

const passport = require('passport');

const pool = require('../databasePool');
const conn = require('../databaseConn');
const { isLoggedIn, hasEmployeePermission, passwordSection } = require('../lib/authentication');

const path = require('path');
const multer = require('multer');

const fs = require('fs');
const { v4: uuidv4 } = require('uuid')
let ruta = ''

const storage = multer.diskStorage({
    destination: path.join(__dirname, '../public/img/employee'),
    filename: (req, file, cb) => {
        ruta = uuidv4() + path.extname(file.originalname).toLocaleLowerCase();
        cb(null, ruta);
    }
});

/*---------------Edit user info---------------*/
//Agregar parámetros de isLogged y rol
//See basic information
router.get('/info', isLoggedIn, hasEmployeePermission, async (req, res) => {
    const rows = await pool.query(
        'SELECT idEmpleado, empleado.Nombre, Apellido, rol.Nombre as cargo, RFC, Imagen ' +
        'FROM usuario ' +
        'INNER JOIN empleado ON idUsuario = Usuario ' +
        'INNER JOIN rol ON idRol = usuario.Rol ' +
        'WHERE usuario.idUsuario = ?', [req.user.idUsuario]
    );
    res.render('profile/view', { profile: rows[0] });
});

//Edit basic information
router.post('/info', async (req, res) => {
    const { Nombre, Apellido, Correo } = req.body;
    const empleado = {
        Nombre,
        Apellido
    };

    try {
        /*await conn.beginTransaction(async (err) => {
            if (err) { throw err; }
            await conn.query('UPDATE usuario SET correo = ? WHERE idUsuario = ?', [correo, req.user.idUsuario], async (error, results, fields) => {
                if (error) {
                    return await conn.rollback(async () => {
                        await conn.destroy();
                        throw error;
                    });
                }

                await conn.query('UPDATE empleado SET ? WHERE usuario = ?', [empleado, req.user.idUsuario], async (error, results, fields) => {
                    if (error) {
                        return await conn.rollback(async () => {
                            await conn.destroy();
                            throw error;
                        });
                    }
                    await conn.commit(async (err) => {
                        if (err) {
                            return await conn.rollback(async () => {
                                await conn.destroy();
                                throw error;
                            });
                        }
                        console.log('success!');
                        await conn.destroy();
                    });
                });
            });
        });

        //await pool.query('UPDATE link SET ? WHERE linkId = ?', [editLink, id]);
        req.flash('success', 'Se han modificado los datos');*/

        await pool.query('UPDATE usuario SET Correo = ? WHERE idUsuario = ?', [Correo, req.user.idUsuario]);
        await pool.query('UPDATE empleado SET ? WHERE Usuario = ?', [empleado, req.user.idUsuario]);
        //await pool.query('UPDATE link SET ? WHERE linkId = ?', [editLink, id]);
        req.flash('success', 'Se han modificado sus datos');


    } catch (error) {
        req.flash('error', 'No se han podido modificar los datos');
    }
    finally {
        res.redirect('/profile/info');
    }

});

//Edit profile photo
const photo = multer({
    storage: storage,
    dest: path.join(__dirname, '../public/img/employee'),
    limits: { fileSize: 2000000 },
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/;
        const mimetype = fileTypes.test(file.mimetype);
        const extName = fileTypes.test(path.extname(file.originalname));
        if (mimetype && extName) {
            return cb(null, true);
        }
        cb('Error: File must be an image');
    }
}).single('profile-foto');

router.post('/photo', photo, async (req, res) => {
    const rows = await pool.query('SELECT idEmpleado, Imagen FROM empleado WHERE Usuario = ?', [req.user.idUsuario]);
    if (rows.length > 0) {
        const empleado = rows[0];
        let imagen = rows[0].Imagen;
        if (imagen !== null && imagen !== '') {
            var filePath = path.join(__dirname, `../public${imagen}`);
            fs.unlinkSync(filePath);
        }

        let Imagen = `/img/employee/${ruta}`;

        await pool.query('UPDATE empleado SET Imagen = ? WHERE idEmpleado = ?', [Imagen, empleado.idEmpleado]);
        req.flash("success", "Se ha modificado la foto correctamente");
        res.redirect('/profile/info');
    }
});

/*---------------Edit password---------------*/
router.get('/password', isLoggedIn, passwordSection, (req, res) => {
    res.render('profile/editPassword');
});

router.post('/password', (req, res, next) => {
    passport.authenticate('profile.editPassword', {
        successRedirect: '/home',
        failureRedirect: '/profile/password',
        failureFlash: true
    })(req, res, next);
});

module.exports = router;