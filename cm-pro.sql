-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-12-2020 a las 20:46:03
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cm-pro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `atributo`
--

CREATE TABLE `atributo` (
  `idAtributo` int(11) NOT NULL,
  `Atributo` varchar(30) NOT NULL,
  `Descripcion_IyE` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `idBitacora` int(11) NOT NULL,
  `Movimiento` varchar(50) NOT NULL,
  `Empleado` varchar(45) NOT NULL,
  `Tabla` varchar(45) NOT NULL,
  `FechaYHora` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `idMovimiento` int(11) NOT NULL,
  `InformacionAnterior` text NOT NULL,
  `InformacionActual` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carteradeservicios`
--

CREATE TABLE `carteradeservicios` (
  `idCdeServicios` int(11) NOT NULL,
  `TiposServicio` int(11) NOT NULL,
  `Servicio` varchar(60) NOT NULL,
  `Costo` float NOT NULL,
  `Descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `idCiudad` int(11) NOT NULL,
  `Estado` int(11) NOT NULL,
  `Ciudad` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Apellido` varchar(45) NOT NULL,
  `RazonSocial` varchar(50) NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `AgregadoPor` int(11) NOT NULL,
  `RFC` varchar(13) NOT NULL,
  `Imagen` text NOT NULL,
  `Usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion` 
--

CREATE TABLE `cotizacion` (
  `idCotizacion` int(11) NOT NULL,
  `Proyecto` int(11) NOT NULL,
  `Servicio` int(11) NOT NULL,
  `Costo` float NOT NULL,
  `Checklist` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacionhasservicio`
--

CREATE TABLE `cotizacionhasservicio` (
  `idCHS` int(11) NOT NULL,
  `Servicio` int(11) NOT NULL,
  `Cotizacion` int(11) NOT NULL,
  `Costo` float NOT NULL,
  `Descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE `cuenta` (
  `idCuenta` int(11) NOT NULL,
  `Proveedor` int(11) NOT NULL,
  `Cuenta` varchar(18) NOT NULL,
  `SWIFT` varchar(20) NOT NULL,
  `Banco` int(11) NOT NULL,
  `CLABE` varchar(18) NOT NULL,
  `Tarjeta` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion`
--

CREATE TABLE `direccion` (
  `idDireccion` int(11) NOT NULL,
  `Ciudad` int(11) NOT NULL,
  `Colonia` varchar(45) NOT NULL,
  `Calle` varchar(45) NOT NULL,
  `Numero` varchar(4) NOT NULL,
  `CP` varchar(5) NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `Ubicacion` geometry NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `idEmpleado` int(11) NOT NULL,
  `Usuario` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Apellido` varchar(45) NOT NULL,
  `RFC` varchar(13) NOT NULL,
  `Imagen` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `idEstado` int(11) NOT NULL,
  `Estado` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formadepago`
--

CREATE TABLE `formadepago` (
  `idFdePago` int(11) NOT NULL,
  `Forma` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genteproyecto`
--

CREATE TABLE `genteproyecto` (
  `idGP` int(11) NOT NULL,
  `Proyecto` int(11) NOT NULL,
  `Ayudante` int(11) NOT NULL,
  `Porcentaje` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `iye`
--

CREATE TABLE `iye` (
  `idIyE` int(11) NOT NULL,
  `Tipo` enum('Ingreso','Egreso') NOT NULL,
  `Descripcion` text NOT NULL,
  `Cantidad` float NOT NULL,
  `AgregadoPor` int(11) NOT NULL,
  `Fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `Servicio` int(11) NOT NULL,
  `Proyecto` int(11) NOT NULL,
  `FormaDePago` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulo`
--

CREATE TABLE `modulo` (
  `idModulo` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operaciones`
--

CREATE TABLE `operaciones` (
  `idOperacion` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `idModulo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idProveedor` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Tipo` enum('Material','Intelectual') NOT NULL,
  `Descripcion` text NOT NULL,
  `Correo` varchar(30) NOT NULL,
  `Telefono` varchar(10) NOT NULL,
  `AgregadoPor` int(11) NOT NULL,
  `RFC` varchar(13) NOT NULL,
  `Score` float NOT NULL,
  `Direccion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedorhasservicio`
--

CREATE TABLE `proveedorhasservicio` (
  `idPHS` int(11) NOT NULL,
  `Proveedor` int(11) NOT NULL,
  `Servicio` int(11) NOT NULL,
  `Costo` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedorhasserviciohasatributp`
--

CREATE TABLE `proveedorhasserviciohasatributp` (
  `idPHSHA` int(11) NOT NULL,
  `Servicio` int(11) NOT NULL,
  `Atributo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyecto`
--

CREATE TABLE `proyecto` (
  `idProyecto` int(11) NOT NULL,
  `Proyecto` varchar(45) NOT NULL,
  `Encargado` int(11) NOT NULL,
  `Direccion` int(11) NOT NULL,
  `Fecha_Inicio` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `Fecha_TenFin` date NOT NULL,
  `Fecha_Fin` datetime NOT NULL,
  `Presupuesto` float NOT NULL,
  `Cliente` int(11) NOT NULL,
  `TipoServicio` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Contrato` text NOT NULL,
  `Imagen` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectohasservicio`
--

CREATE TABLE `proyectohasservicio` (
  `idProyHServ` int(11) NOT NULL,
  `Servicio` int(11) NOT NULL,
  `Encargado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idRol` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolhasoperacion`
--

CREATE TABLE `rolhasoperacion` (
  `idRol_Op` int(11) NOT NULL,
  `idRol` int(11) NOT NULL,
  `idOperacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serviciohasatributo`
--

CREATE TABLE `serviciohasatributo` (
  `idSHA` int(11) NOT NULL,
  `Atributo` int(11) NOT NULL,
  `Servicio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicioproveedores`
--

CREATE TABLE `servicioproveedores` (
  `idServicioProveedores` int(11) NOT NULL,
  `Proveedor` int(11) NOT NULL,
  `Servicio` varchar(60) NOT NULL,
  `Tipo` int(11) NOT NULL,
  `Costo` float NOT NULL,
  `Descripcion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposervicio`
--

CREATE TABLE `tiposervicio` (
  `idTiposServicio` int(11) NOT NULL,
  `Servicio` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Contrasena` varchar(60) NOT NULL,
  `Rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `atributo`
--
ALTER TABLE `atributo`
  ADD PRIMARY KEY (`idAtributo`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`idBitacora`);

--
-- Indices de la tabla `carteradeservicios`
--
ALTER TABLE `carteradeservicios`
  ADD PRIMARY KEY (`idCdeServicios`),
  ADD KEY `Rel_Cartera_TipoServicio` (`TiposServicio`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`idCiudad`),
  ADD KEY `Rel_Ciudad_Estado` (`Estado`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`),
  ADD KEY `Rel_Cliente_Empleado` (`AgregadoPor`),
  ADD KEY `Rel_Cliente_Usuario` (`Usuario`);

--
-- Indices de la tabla `cotizacion` 
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`idCotizacion`),
  ADD KEY `Rel_Cotizacion_Proyecto` (`Proyecto`),
  ADD KEY `Rel_Cotizacion_Servicio` (`Servicio`);

--
-- Indices de la tabla `cotizacionhasservicio`
--
ALTER TABLE `cotizacionhasservicio`
  ADD PRIMARY KEY (`idCHS`),
  ADD KEY `Rel_CHS_Cotizacion` (`Cotizacion`),
  ADD KEY `Rel_CHS_PHS` (`Servicio`);

--
-- Indices de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`idCuenta`),
  ADD KEY `Rel_Cuenta_Proveedor` (`Proveedor`);

--
-- Indices de la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD PRIMARY KEY (`idDireccion`),
  ADD KEY `Rel_Direccion_Ciudad` (`Ciudad`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idEmpleado`),
  ADD KEY `Rel_Empleado_Usuario` (`Usuario`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idEstado`);

--
-- Indices de la tabla `formadepago`
--
ALTER TABLE `formadepago`
  ADD PRIMARY KEY (`idFdePago`);

--
-- Indices de la tabla `genteproyecto`
--
ALTER TABLE `genteproyecto`
  ADD PRIMARY KEY (`idGP`),
  ADD KEY `Rel_GProyecto_Proyecto` (`Proyecto`),
  ADD KEY `Rel_GProyecto_Empleado` (`Ayudante`);

--
-- Indices de la tabla `iye`
--
ALTER TABLE `iye`
  ADD PRIMARY KEY (`idIyE`),
  ADD KEY `Rel_IyE_Empleado` (`AgregadoPor`),
  ADD KEY `Rel_IyE_TipoServicio` (`Servicio`),
  ADD KEY `Rel_IyE_Proyecto` (`Proyecto`),
  ADD KEY `Rel_IyE_FDePago` (`FormaDePago`);

--
-- Indices de la tabla `modulo`
--
ALTER TABLE `modulo`
  ADD PRIMARY KEY (`idModulo`);

--
-- Indices de la tabla `operaciones`
--
ALTER TABLE `operaciones`
  ADD PRIMARY KEY (`idOperacion`),
  ADD KEY `Rel_Operaciones_Modulo` (`idModulo`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`),
  ADD KEY `Rel_Proveedor_Direccion` (`Direccion`);

--
-- Indices de la tabla `proveedorhasservicio`
--
ALTER TABLE `proveedorhasservicio`
  ADD PRIMARY KEY (`idPHS`),
  ADD KEY `Rel_ProvHasServ_Proveedor` (`Proveedor`),
  ADD KEY `Rel_ProvHasServ_ServProv` (`Servicio`);

--
-- Indices de la tabla `proveedorhasserviciohasatributp`
--
ALTER TABLE `proveedorhasserviciohasatributp`
  ADD PRIMARY KEY (`idPHSHA`),
  ADD KEY `Rel_PHSHA_ProvHasServ` (`Servicio`),
  ADD KEY `Rel_PHSHA_Atributo` (`Atributo`);

--
-- Indices de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD PRIMARY KEY (`idProyecto`),
  ADD KEY `Rel_Proyecto_Empleado` (`Encargado`),
  ADD KEY `Rel_Proyecto_Direccion` (`Direccion`),
  ADD KEY `Rel_Proyecto_Cliente` (`Cliente`);

--
-- Indices de la tabla `proyectohasservicio`
--
ALTER TABLE `proyectohasservicio`
  ADD PRIMARY KEY (`idProyHServ`),
  ADD KEY `Rel_PHS_GProyecto` (`Encargado`),
  ADD KEY `Rel_PHS_CarDeServicios` (`Servicio`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `rolhasoperacion`
--
ALTER TABLE `rolhasoperacion`
  ADD PRIMARY KEY (`idRol_Op`),
  ADD KEY `Rel_RHO_Rol` (`idRol`),
  ADD KEY `Rel_RHO_Operaciones` (`idOperacion`);

--
-- Indices de la tabla `serviciohasatributo`
--
ALTER TABLE `serviciohasatributo`
  ADD PRIMARY KEY (`idSHA`),
  ADD KEY `Rel_SHA_Atributo` (`Atributo`),
  ADD KEY `Rel_SHA_CarDeServ` (`Servicio`);

--
-- Indices de la tabla `servicioproveedores`
--
ALTER TABLE `servicioproveedores`
  ADD PRIMARY KEY (`idServicioProveedores`),
  ADD KEY `Rel_ServProv_Prov` (`Proveedor`),
  ADD KEY `Rel_ServProv_TipoServ` (`Tipo`);

--
-- Indices de la tabla `tiposervicio`
--
ALTER TABLE `tiposervicio`
  ADD PRIMARY KEY (`idTiposServicio`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `Rel_Usuario_Rol` (`Rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `atributo`
--
ALTER TABLE `atributo`
  MODIFY `idAtributo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `idBitacora` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `carteradeservicios`
--
ALTER TABLE `carteradeservicios`
  MODIFY `idCdeServicios` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `idCiudad` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `idCotizacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cotizacionhasservicio`
--
ALTER TABLE `cotizacionhasservicio`
  MODIFY `idCHS` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `idCuenta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `direccion`
--
ALTER TABLE `direccion`
  MODIFY `idDireccion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idEmpleado` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `idEstado` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `formadepago`
--
ALTER TABLE `formadepago`
  MODIFY `idFdePago` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `genteproyecto`
--
ALTER TABLE `genteproyecto`
  MODIFY `idGP` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `iye`
--
ALTER TABLE `iye`
  MODIFY `idIyE` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `modulo`
--
ALTER TABLE `modulo`
  MODIFY `idModulo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `operaciones`
--
ALTER TABLE `operaciones`
  MODIFY `idOperacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proveedorhasservicio`
--
ALTER TABLE `proveedorhasservicio`
  MODIFY `idPHS` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proveedorhasserviciohasatributp`
--
ALTER TABLE `proveedorhasserviciohasatributp`
  MODIFY `idPHSHA` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proyecto`
--
ALTER TABLE `proyecto`
  MODIFY `idProyecto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proyectohasservicio`
--
ALTER TABLE `proyectohasservicio`
  MODIFY `idProyHServ` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rolhasoperacion`
--
ALTER TABLE `rolhasoperacion`
  MODIFY `idRol_Op` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `serviciohasatributo`
--
ALTER TABLE `serviciohasatributo`
  MODIFY `idSHA` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicioproveedores`
--
ALTER TABLE `servicioproveedores`
  MODIFY `idServicioProveedores` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tiposervicio`
--
ALTER TABLE `tiposervicio`
  MODIFY `idTiposServicio` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carteradeservicios`
--
ALTER TABLE `carteradeservicios`
  ADD CONSTRAINT `Rel_Cartera_TipoServicio` FOREIGN KEY (`TiposServicio`) REFERENCES `tiposervicio` (`idTiposServicio`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `Rel_Ciudad_Estado` FOREIGN KEY (`Estado`) REFERENCES `estado` (`idEstado`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `Rel_Cliente_Empleado` FOREIGN KEY (`AgregadoPor`) REFERENCES `empleado` (`idEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_Cliente_Usuario` FOREIGN KEY (`Usuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cotizacion` 
--
ALTER TABLE `cotizacion`
  ADD CONSTRAINT `Rel_Cotizacion_Proyecto` FOREIGN KEY (`Proyecto`) REFERENCES `proyecto` (`idProyecto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_Cotizacion_Servicio` FOREIGN KEY (`Servicio`) REFERENCES `carteradeservicios` (`idCdeServicios`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cotizacionhasservicio`
--
ALTER TABLE `cotizacionhasservicio`
  ADD CONSTRAINT `Rel_CHS_Cotizacion` FOREIGN KEY (`Cotizacion`) REFERENCES `cotizacion` (`idCotizacion`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_CHS_PHS` FOREIGN KEY (`Servicio`) REFERENCES `proyectohasservicio` (`idProyHServ`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `Rel_Cuenta_Proveedor` FOREIGN KEY (`Proveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD CONSTRAINT `Rel_Direccion_Ciudad` FOREIGN KEY (`Ciudad`) REFERENCES `ciudad` (`idCiudad`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `Rel_Empleado_Usuario` FOREIGN KEY (`Usuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `genteproyecto`
--
ALTER TABLE `genteproyecto`
  ADD CONSTRAINT `Rel_GProyecto_Empleado` FOREIGN KEY (`Ayudante`) REFERENCES `empleado` (`idEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_GProyecto_Proyecto` FOREIGN KEY (`Proyecto`) REFERENCES `proyecto` (`idProyecto`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `iye`
--
ALTER TABLE `iye`
  ADD CONSTRAINT `Rel_IyE_Empleado` FOREIGN KEY (`AgregadoPor`) REFERENCES `empleado` (`idEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_IyE_FDePago` FOREIGN KEY (`FormaDePago`) REFERENCES `formadepago` (`idFdePago`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_IyE_Proyecto` FOREIGN KEY (`Proyecto`) REFERENCES `proyecto` (`idProyecto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_IyE_TipoServicio` FOREIGN KEY (`Servicio`) REFERENCES `tiposervicio` (`idTiposServicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `operaciones`
--
ALTER TABLE `operaciones`
  ADD CONSTRAINT `Rel_Operaciones_Modulo` FOREIGN KEY (`idModulo`) REFERENCES `modulo` (`idModulo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `Rel_Proveedor_Direccion` FOREIGN KEY (`Direccion`) REFERENCES `direccion` (`idDireccion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proveedorhasservicio`
--
ALTER TABLE `proveedorhasservicio`
  ADD CONSTRAINT `Rel_ProvHasServ_Proveedor` FOREIGN KEY (`Proveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_ProvHasServ_ServProv` FOREIGN KEY (`Servicio`) REFERENCES `servicioproveedores` (`idServicioProveedores`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proveedorhasserviciohasatributp`
--
ALTER TABLE `proveedorhasserviciohasatributp`
  ADD CONSTRAINT `Rel_PHSHA_Atributo` FOREIGN KEY (`Atributo`) REFERENCES `atributo` (`idAtributo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_PHSHA_ProvHasServ` FOREIGN KEY (`Servicio`) REFERENCES `servicioproveedores` (`idServicioProveedores`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proyecto`
--
ALTER TABLE `proyecto`
  ADD CONSTRAINT `Rel_Proyecto_Cliente` FOREIGN KEY (`Cliente`) REFERENCES `cliente` (`idCliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_Proyecto_Direccion` FOREIGN KEY (`Direccion`) REFERENCES `direccion` (`idDireccion`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_Proyecto_Empleado` FOREIGN KEY (`Encargado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proyectohasservicio`
--
ALTER TABLE `proyectohasservicio`
  ADD CONSTRAINT `Rel_PHS_CarDeServicios` FOREIGN KEY (`Servicio`) REFERENCES `carteradeservicios` (`idCdeServicios`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_PHS_GProyecto` FOREIGN KEY (`Encargado`) REFERENCES `genteproyecto` (`idGP`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `rolhasoperacion`
--
ALTER TABLE `rolhasoperacion`
  ADD CONSTRAINT `Rel_RHO_Operaciones` FOREIGN KEY (`idOperacion`) REFERENCES `operaciones` (`idOperacion`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_RHO_Rol` FOREIGN KEY (`idRol`) REFERENCES `rol` (`idRol`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `serviciohasatributo`
--
ALTER TABLE `serviciohasatributo`
  ADD CONSTRAINT `Rel_SHA_Atributo` FOREIGN KEY (`Atributo`) REFERENCES `atributo` (`idAtributo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_SHA_CarDeServ` FOREIGN KEY (`Servicio`) REFERENCES `carteradeservicios` (`idCdeServicios`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `servicioproveedores`
--
ALTER TABLE `servicioproveedores`
  ADD CONSTRAINT `Rel_ServProv_Prov` FOREIGN KEY (`Proveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Rel_ServProv_TipoServ` FOREIGN KEY (`Tipo`) REFERENCES `tiposervicio` (`idTiposServicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `Rel_Usuario_Rol` FOREIGN KEY (`Rol`) REFERENCES `rol` (`idRol`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
